<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your html goes here -->

<section id="content_section" class="content">
    <!-- Your Page Content Here -->
    <div>

        <p><a title="Return" href="/admin/pedidos"><i class="fa fa-chevron-circle-left "></i>
                &nbsp; Voltar a lista de pedidos</a></p>

        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><i class="fa fa-shopping-basket"></i> Detalhes de Pedidos</strong>
            </div>

            <div class="panel-body" style="padding:20px 0px 0px 0px">
                <form class="form-horizontal" method="post" id="form" enctype="multipart/form-data"
                    action="/admin/produtos/edit-save/1982">
                    @csrf
                    <input type="hidden" name="return_url" value="/admin/produtos">
                    <input type="hidden" name="ref_mainpath" value="/admin/produtos">
                    <input type="hidden" name="ref_parameter" value="return_url=/admin/produtos">
                    <div class="box-body" id="parent-form-area">
                        
                        <div class="table-responsive">
                            <table id="table-detail" class="table table-striped">
                                <tbody>
                                  
                                    @foreach ($cliente as $item)
                                        <tr>
                                            <td>CNPJ</td>
                                            <td>{{$item->cnpj}}</td>
                                        </tr>
                                        
                                        <tr>
                                          <td>Código do Cliente</td>
                                          <td>{{$item->codigo_cliente}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td>Nome do Responsável</td>
                                            <td>{{$item->nome_completo}}</td>
                                        </tr>

                                        <tr>
                                          <td>Email</td>
                                          <td>{{$item->email}}</td>
                                        </tr>

                                        <tr>
                                          <td>Telefone Fixo</td>
                                          <td>{{$item->telefone_fixo}}</td>
                                        </tr>

                                        <tr>
                                          <td>Telefone Celular</td>
                                          <td>{{$item->telefone_celular}}</td>
                                        </tr>

                                        <tr>
                                            <td>Endereço</td>
                                            <td>
                                              {{$item->endereco}}, {{$item->numero}}, {{$item->bairro}}, 
                                              {{$item->cidade}}, {{$item->cep}} - {{$item->estado}}  </td>
                                        </tr>
                                    @endforeach
                                        <tr>
                                          <td>Status</td>
                                          <td>{{$pedidos[0]->status}}</td>
                                        </tr>

                                        <tr>
                                            <td>Motivo de Recusão</td>
                                            <td>{{$pedidos[0]->motivo_recusao_cotacao}} </td>
                                        </tr>

                                </tbody>
                            </table>

                            <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col">Nome</th>
                                  <th scope="col">Marca</th>
                                  <th scope="col">Código do Produto</th>
                                  <th scope="col">Quantidade</th>
                                  <th scope="col">Aceito pelo cliente</th>
                                  <th scope="col">Preço</th>
                                  <th scope="col">Subtotal</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach ($produtos as $item)
                                <tr>
                                  <td>{{$item->nome}}</td>
                                  <td>{{$item->marca}}</td>
                                  <td>{{$item->codigo_produto}}</td>
                                  <td>{{$item->quantidade}}</td>
                                  <td>
                                    @if($item->aceito_pelo_cliente == 'S')
                                      Sim
                                    @else 
                                      Não
                                    @endif
                                    
                                  </td>
                                  <td>R$ {{ number_format($item->preco, 2, ',', '.') }}</td>
                                  <td>R$ {{ number_format($item->subtotal, 2, ',', '.') }}</td>
                                </tr>
                                @endforeach
                                <tr>
                                  <td><b>TOTAL</b></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td><b>R$ {{$valor_total}}</b></td>
                                </tr>
                                
                              </tbody>
                            </table>
                        </div>

                    </div><!-- /.box-body -->


                </form>

            </div>
        </div>
    </div>
    <!--END AUTO MARGIN-->

</section>




{{-- <div class='panel panel-default'>
    <div class='panel-heading'>Edit Form</div>
    <div class='panel-body'>
        <div class='form-group'>
            <label>Name</label>

        </div>
        <p>{{$pedidos}}</p>

        <p> {{$cliente}}</p>

        @foreach ($produtos as $item)
        <p>{{$item->nome}}</p>
        @endforeach
        <!-- etc .... -->

        </form>
    </div>
</div> --}}
@endsection
