@extends('layouts.app')

@section('content')

<!-- Main layout -->
<main>
    <div class="container" ng-controller="perfilController">

        <!-- Section: Customers -->
        <section class="section team-section">

            <div class="row mb-1">
                <div class="col-md-9">
                    <a class="btn btn-primary" href="/" style="margin-left: 0px;">
                        Criar Nova Cotação
                    </a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editarPerfil">
                        Editar Perfil
                    </button>
                </div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="editarPerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Editar Minhas Informações</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                            <form method="POST" action="{{ route('editarCadastro') }}" name="formRegister">
                                @csrf

                                <!-- Section: Contact v.3 -->
                                <section class="contact-section my-5">


                                    <!-- Form with header -->
                                    <div class="card">

                                        <!-- Grid row -->
                                        <div class="row">

                                            <!-- Grid column -->
                                            <div class="col-lg-12">

                                                <div class="card-body form">
                                                    <input name="id" ng-model="userData.id" type="text" id="id"
                                                                hidden  class="form-control" ng-model >
                                                    <!-- Header -->
                                                    <h3 class="mt-4 text-center">Meu Cadastro</h3>

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-6">

                                                            <div class="md-form mb-0">

                                                                <input name="cnpj" mask="00.000.000/0000-00"
                                                                    ng-model="userData.cnpj" type="text" id="cnpj"
                                                                    class="form-control" ng-model required ui-br-cnpj-mask oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="cnpj" class="" ng-class="{'active': userData.cnpj !== undefined}">CNPJ *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-6">

                                                            <div class="md-form mb-0">

                                                                <input name="email" ng-model="userData.email" type="email"
                                                                    id="email" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="email" class="" ng-class="{'active': userData.email !== undefined}">Seu email *</label>m
                                                    
                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                
                                                </div>

                                            </div>
                                            <!-- Grid column -->

                                        </div>
                                        <!-- Grid row -->

                                    </div>
                                    <!-- Form with header -->

                                </section>
                                <!-- Section: Contact v.3 -->

                                <!-- Section: Contact v.3 -->
                                <section class="contact-section my-5">

                                    <!-- Form with header -->
                                    <div class="card">

                                        <!-- Grid row -->
                                        <div class="row">

                                            <!-- Grid column -->
                                            <div class="col-lg-12">

                                                <div class="card-body form">

                                                    <!-- Header -->
                                                    <h3 class="mt-4 text-center">Dados da Empresa</h3>

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-6">

                                                            <div class="md-form mb-0">

                                                                <input name="razao_social" ng-model="userData.razao_social"
                                                                    type="text" id="razao_social" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="razao_social" class="" ng-class="{'active': userData.razao_social !== undefined}">Razão Social *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-6">

                                                            <div class="md-form mb-0">

                                                                <input name="inscricao_estadual"
                                                                    ng-model="userData.inscricao_estadual" type="text"
                                                                    id="inscricao_estadual" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="inscricao_estadual" class="" ng-class="{'active': userData.inscricao_estadual !== undefined}">Inscrição Estadual
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-6">

                                                            <div class="md-form mb-0">

                                                                <input name="nome_fantasia"
                                                                    ng-model="userData.nome_fantasia" type="text"
                                                                    id="nome_fantasia" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="nome_fantasia" class="" ng-class="{'active': userData.nome_fantasia !== undefined}">Nome Fantasia
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-6">

                                                            <div class="md-form mb-0">

                                                                <input name="ramo_atividade" ng-model="userData.ramo_atividade"
                                                                    type="text" id="ramo_atividade" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="ramo_atividade" class="" ng-class="{'active': userData.ramo_atividade !== undefined}">Ramo de Atividade *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="cep" ng-model="userData.cep"
                                                                    ng-change="getEndereco()" type="text" id="cep"
                                                                    class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="cep" class="" ng-class="{'active': userData.cep !== undefined}">CEP *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="estado" ng-model="userData.estado" type="text"
                                                                    id="estado" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="estado" class=""
                                                                    ng-class="{'active': userData.estado !== undefined}">Estado
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="cidade" ng-model="userData.cidade" type="text"
                                                                    id="cidade" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="cidade" class=""
                                                                    ng-class="{'active': userData.cidade !== undefined}">Cidade
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="bairro" ng-model="userData.bairro" type="text"
                                                                    id="bairro" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="bairro" class=""
                                                                    ng-class="{'active': userData.bairro !== undefined}">Bairro
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="endereco" ng-model="userData.endereco" type="text"
                                                                    id="endereco" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="endereco" class=""
                                                                    ng-class="{'active': userData.endereco !== undefined}">Endereço
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="numero" ng-model="userData.numero" type="text"
                                                                    id="numero" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="numero" class="" ng-class="{'active': userData.numero !== undefined}">Número *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="complemento" ng-model="userData.complemento"
                                                                    type="text" id="complemento" class="form-control">

                                                                <label for="complemento" class=""
                                                                    ng-class="{'active': userData.complemento !== undefined}">Complemento</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="ponto_referencia"
                                                                    ng-model="userData.ponto_referencia" type="text"
                                                                    id="ponto_referencia" class="form-control">

                                                                <label for="ponto_referencia" class="" ng-class="{'active': userData.ponto_referencia !== undefined}">Ponto de
                                                                    Referência</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="quantos_funcionario"
                                                                    ng-model="userData.quantos_funcionario" type="text"
                                                                    id="quantos_funcionario" class="form-control">

                                                                <label for="quantos_funcionario" class="" ng-class="{'active': userData.quantos_funcionario !== undefined}">Quantos
                                                                    Funcionários</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->


                                                </div>

                                            </div>
                                            <!-- Grid column -->

                                        </div>
                                        <!-- Grid row -->

                                    </div>
                                    <!-- Form with header -->

                                </section>
                                <!-- Section: Contact v.3 -->

                                <!-- Section: Contact v.3 -->
                                <section class="contact-section my-5">

                                    <!-- Form with header -->
                                    <div class="card">

                                        <!-- Grid row -->
                                        <div class="row">

                                            <!-- Grid column -->
                                            <div class="col-lg-12">

                                                <div class="card-body form">

                                                    <!-- Header -->
                                                    <h3 class="mt-4 text-center">Dados do Comprador</h3>

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="nome_completo" ng-model="userData.nome_completo"
                                                                    type="text" id="nome_completo" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="nome_completo" class="" ng-class="{'active': userData.nome_completo !== undefined}">Nome Completo *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="data_nascimento" ng-model="userData.data_nascimento"
                                                                    type="text" id="data_nascimento" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="data_nascimento" class="" ng-class="{'active': userData.data_nascimento !== undefined}">Data de Nascimento
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">
                                                                <label for="sexo" class="active">Sexo *</label>
                                                                <div class="form-check form-check-inline">
                                                                    <input type="radio" class="form-check-input" id="sexo1"
                                                                        name="sexo" value="Feminino" ng-model="userData.sexo">
                                                                    <label class="form-check-label" for="sexo1" >Feminino</label>
                                                                </div>

                                                                <!-- Material inline 2 -->
                                                                <div class="form-check form-check-inline">
                                                                    <input type="radio" class="form-check-input" id="sexo2"
                                                                        name="sexo" value="Masculino" ng-model="userData.sexo">
                                                                    <label class="form-check-label"
                                                                        for="sexo2">Masculino</label>
                                                                </div>

                                                                <!--Blue select-->

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="profissao" ng-model="userData.profissao" type="text"
                                                                    id="profissao" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="profissao" class="" ng-class="{'active': userData.profissao !== undefined}">Cargo *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="telefone_fixo" ng-model="userData.telefone_fixo" ui-br-phone-number type="text"
                                                                    id="telefone_fixo" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="telefone_fixo" class="" ng-class="{'active': userData.telefone_fixo !== undefined}">Telefone Fixo *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="telefone_celular"
                                                                ng-model="userData.telefone_celular" ui-br-phone-number type="text"
                                                                    id="telefone_celular" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="telefone_celular" class="" ng-class="{'active': userData.telefone_celular !== undefined}">Telefone Celular
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                </div>

                                            </div>
                                            <!-- Grid column -->

                                        </div>
                                        <!-- Grid row -->

                                    </div>
                                    <!-- Form with header -->

                                </section>
                                <!-- Section: Contact v.3 -->

                                <!-- Section: Contact v.3 -->
                                <section class="contact-section my-5">

                                    <!-- Form with header -->
                                    <div class="card">

                                        <!-- Grid row -->
                                        <div class="row">

                                            <!-- Grid column -->
                                            <div class="col-lg-12">

                                                <div class="card-body form">

                                                    <!-- Header -->
                                                    <h3 class="mt-4 text-center">Dados da Entrega</h3>

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="cep_entrega" ng-model="userData.cep_entrega"
                                                                    ng-change="getEnderecoEntrega()" type="text" id="cep_entrega"
                                                                    class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="cep_entrega" ng-class="{'active': userData.cep_entrega !== undefined}">CEP *</label>


                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="estado_entrega" ng-model="userData.estado_entrega"
                                                                    type="text" id="estado_entrega" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="estado_entrega" class=""
                                                                    ng-class="{'active': userData.estado_entrega !== undefined}">Estado
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="cidade_entrega" ng-model="userData.cidade_entrega"
                                                                    type="text" id="cidade_entrega" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="cidade_entrega" class=""
                                                                    ng-class="{'active': userData.cidade_entrega !== undefined}">Cidade
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="bairro_entrega" ng-model="userData.bairro_entrega"
                                                                    type="text" id="bairro_entrega" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="bairro_entrega" class=""
                                                                    ng-class="{'active': userData.bairro_entrega !== undefined}">Bairro
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="endereco_entrega"
                                                                    ng-model="userData.endereco_entrega" type="text"
                                                                    id="endereco_entrega" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="endereco_entrega" class=""
                                                                    ng-class="{'active': userData.endereco_entrega !== undefined}">Endereço
                                                                    *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="numero_entrega" ng-model="userData.numero_entrega"
                                                                    type="text" id="numero_entrega" class="form-control"
                                                                    required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                                    onchange="try{setCustomValidity('')}catch(e){}">

                                                                <label for="numero_entrega" ng-class="{'active': userData.numero_entrega !== undefined}" class="">Número *</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->

                                                    <!-- Grid row -->
                                                    <div class="row">

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="complemento_entrega"
                                                                    ng-model="userData.complemento_entrega" type="text"
                                                                    id="complemento_entrega" class="form-control">

                                                                <label for="complemento_entrega" class=""
                                                                    ng-class="{'active': userData.complemento_entrega !== undefined}">Complemento</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                        <!-- Grid column -->
                                                        <div class="col-md-4">

                                                            <div class="md-form mb-0">

                                                                <input name="ponto_referencia_entrega"
                                                                    ng-model="userData.ponto_referencia_entrega" type="text"
                                                                    id="ponto_referencia_entrega" class="form-control">

                                                                <label for="ponto_referencia_entrega" class="" ng-class="{'active': userData.ponto_referencia_entrega !== undefined}">Ponto de
                                                                    Referência</label>

                                                            </div>

                                                        </div>
                                                        <!-- Grid column -->

                                                    </div>
                                                    <!-- Grid row -->


                                                </div>

                                            </div>
                                            <!-- Grid column -->

                                        </div>
                                        <!-- Grid row -->

                                    </div>
                                    <!-- Form with header -->

                                </section>
                                <!-- Section: Contact v.3 -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Salvar Alterações</button>
                            </div>
                        </div>
                    </form>

                        
                </div>
            </div>   

            <div class="row mb-1">
                <div class="col-md-9">
                    <h4 class="h4-responsive mt-1">Meus Dados</h4>
                </div>

            </div>
            <!-- First row -->
            <div class="row">

                <!-- First column -->
                <div class="col-md-6">


                    <div class="row">
                        <div class="col-md-12 mb-1">
                            <!-- Tabs -->
                            <div class="classic-tabs">
                                <!-- Nav tabs -->
                                <div class="tabs-wrapper">
                                    <ul class="nav tabs-primary primary-color" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link waves-light" data-toggle="tab" href="#panel83"
                                                role="tab">Minhas Cotações</a>
                                        </li>

                                    </ul>
                                </div>
                                <!-- Tab panels -->
                                <div class="tab-content card">
                                    <!-- Panel 1 -->
                                    <div class="tab-pane fade show active" id="panel83" role="tabpanel">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Data da Cotação</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="pedido in pedidos">
                                                        <th scope="row">@{{$index + 1}}</th>
                                                        <td input-change>
                                                            @{{pedido.pedido.created_at | date: "dd/MM/yyyy"}}
                                                        </td>
                                                        <td>
                                                            @{{pedido.pedido.status}}
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-primary"
                                                                data-toggle="modal"
                                                                data-target="#basicExampleModal-@{{pedido.pedido.id}}">
                                                                Ver Mais
                                                            </button>

                                                            <!-- Modal -->
                                                            <div class="modal fade"
                                                                id="basicExampleModal-@{{pedido.pedido.id}}"
                                                                tabindex="-1" role="dialog"
                                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-xl" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title"
                                                                                id="exampleModalLabel" input-change>
                                                                                Produtos da cotação feita em
                                                                                @{{pedido.pedido.created_at | date: "dd/MM/yyyy"}}
                                                                            </h5>
                                                                            <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">

                                                                            <table class="table">     
                                                                                <thead>
                                                                                    <tr scope="col">
                                                                                        Detalhes do Pedido
                                                                                    </tr>
                                                                                </thead>                                                                           
                                                                                <tbody>
                                                                                    <tr ng-show="pedido.pedido.codigo_pedido != null">
                                                                                        <td>Número do Pedido:</td>
                                                                                        <td> @{{pedido.pedido.codigo_pedido}}</td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr ng-show="pedido.pedido.numero_nota_fiscal != null">
                                                                                        <td>Número da Nota Fiscal:</td>
                                                                                        <td> @{{pedido.pedido.numero_nota_fiscal}}</td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Condição de Pagamento:</td>
                                                                                        <td> @{{pedido.pedido.condicao_pagamaento}}</td>
                                                                                        
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Prazo de Entrega:</td>
                                                                                        <td> @{{pedido.pedido.prazo_entrega | date: "dd/MM/yyyy"}}</td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td>Status do Pedido:</td>
                                                                                        <td> @{{pedido.pedido.status}}</td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td>Valor Total do Pedido:</td>
                                                                                        <td>R$ @{{pedido.valor_total}}</td>
                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>

                                                                            {{-- <input type="checkbox" 
                                                                            id="selectAll"
                                                                            ng-model="selectAll" 
                                                                            class="form-check-input"
                                                                            ng-click="checkAll(pedido.produtos)" />
                                                                            <label class="form-check-label" for="selectAll"></label> --}}
                                                                            
                                                                            <table class="table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th ng-show="pedido.pedido.status == 'Respondido'" scope="col">Confirmar Produto</th>
                                                                                        <th scope="col">Nome</th>
                                                                                        <th scope="col">Marca</th>
                                                                                        <th scope="col">Código do
                                                                                            Produto</th>
                                                                                        <th scope="col">Quantidade</th>
                                                                                        <th scope="col">Preço</th>
                                                                                        <th scope="col">Total</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    
                                                                                    <tr
                                                                                        ng-repeat="produto in pedido.produtos">
                                                                                        <td ng-show="pedido.pedido.status == 'Respondido'">
                                                                                            <div ng-if="produto.preco != -1" class="form-check">
                                                                                                <input type="checkbox"
                                                                                                       ng-checked="produto.aceito_pelo_cliente == 'S'"
                                                                                                       class="form-check-input" 
                                                                                                       id="materialUnchecked-@{{produto.pedidos_has_produtos_id}}"
                                                                                                       ng-click="aceitarProduto(produto)">
                                                                                                <label class="form-check-label" for="materialUnchecked-@{{produto.pedidos_has_produtos_id}}"></label>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>@{{produto.nome}}</td>
                                                                                        <td>@{{produto.marca}}</td>
                                                                                        <td>@{{produto.codigo_produto}}
                                                                                        </td>
                                                                                        <td> <span ng-if="produto.preco == -1">Produto Indisponivel</span> <span ng-if="produto.preco != -1">@{{produto.quantidade}}</span> </td>
                                                                                        <td> <span ng-if="produto.preco == -1"> - </span> <span ng-if="produto.preco != -1">@{{produto.preco | currency:'R$':2 }}</span> </td>
                                                                                        <td> <span ng-if="produto.preco == -1"> - </span> <span ng-if="produto.preco != -1">@{{produto.subtotal | currency:'R$':2 }}</span> </td>
                                                                                    </tr>
                                                                                    
                                                                                </tbody>
                                                                            </table>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button"
                                                                            class="btn btn-success"
                                                                            ng-show="pedido.pedido.status == 'Respondido'"
                                                                            ng-click="checkAll(pedido)"
                                                                            data-dismiss="modal">Aceitar Todos os Produtos e concluir cotação</button>

                                                                            <button type="button"
                                                                            class="btn btn-success"
                                                                            ng-show="pedido.pedido.status == 'Respondido'"
                                                                            ng-click="aceitarCotacao(pedido.pedido)"
                                                                            data-dismiss="modal">Aceitar Cotação</button>

                                                                            <button type="button"
                                                                            class="btn btn-success"
                                                                            ng-show="pedido.pedido.status == 'Pedido recusado pelo cliente'"
                                                                            ng-click="reabrirCotacao(pedido.pedido)"
                                                                            data-dismiss="modal">Reabrir Cotação</button>

                                                                            <button type="button"
                                                                            class="btn btn-danger"
                                                                            ng-show="pedido.pedido.status == 'Respondido'"
                                                                            ng-click="recusarCotacao(pedido.pedido)"
                                                                            data-dismiss="modal">Recusar Cotação</button>

                                                                            <button type="button"
                                                                            class="btn btn-success"
                                                                            ng-show="pedido.pedido.status == 'Concluído'"
                                                                            ng-click="repetirCotacao(pedido.pedido)"
                                                                            data-dismiss="modal">Repetir Cotação</button>

                                                                            <button type="button"
                                                                                class="btn btn-outline-danger waves-effect"
                                                                                data-dismiss="modal">Fechar</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.Panel 1 -->
                                </div>
                                <!-- /.Tabs -->
                            </div>
                        </div>
                    </div>

                </div>
                <!-- First column -->

                <!-- Second column -->
                <div class="col-md-6 mb-md-0 mb-5">

                    <!-- Card -->
                    <div class="card profile-card">

                        <div class="card-body pt-0 mt-0">

                            <!--Accordion wrapper-->
                            <div class="accordion md-accordion" id="accordionEx" role="tablist"
                                aria-multiselectable="true">

                                <!-- Accordion card -->
                                <div class="card">

                                    <!-- Card header -->
                                    <div class="card-header" role="tab" id="headingOne1">
                                        <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                                            aria-expanded="true" aria-controls="collapseOne1">
                                            <h5 class="mb-0">
                                                @{{userData.nome_completo}} - @{{userData.profissao}}<i
                                                    class="fas fa-angle-down rotate-icon"></i>
                                            </h5>
                                        </a>
                                        
                                    </div>

                                    <!-- Card body -->
                                    <div id="collapseOne1" class="collapse" role="tabpanel"
                                        aria-labelledby="headingOne1" data-parent="#accordionEx">
                                        <div class="card-body">
                                            <ul class="striped list-group">

                                                <li class="list-group-item"><strong>CNPJ:</strong> @{{userData.cnpj}}
                                                </li>

                                                <li class="list-group-item"><strong>E-mail:</strong> @{{userData.email}}
                                                </li>

                                                <li class="list-group-item"><strong>Razão Social:</strong>
                                                    @{{userData.razao_social}}</li>

                                                <li class="list-group-item"><strong>Inscrição Estadual:</strong>
                                                    @{{userData.inscricao_estadual}}</li>

                                                <li class="list-group-item"><strong>Ramo de Atividade:</strong>
                                                    @{{userData.ramo_atividade}}</li>

                                                <li class="list-group-item"><strong>Nome Fantasia:</strong>
                                                    @{{userData.nome_fantasia}}</li>

                                                <li class="list-group-item"><strong>CEP:</strong> @{{userData.cep}}</li>

                                                <li class="list-group-item"><strong>Estado:</strong>
                                                    @{{userData.estado}}</li>

                                                <li class="list-group-item"><strong>Cidade:</strong>
                                                    @{{userData.cidade}}</li>

                                                <li class="list-group-item"><strong>Bairro:</strong>
                                                    @{{userData.bairro}}</li>

                                                <li class="list-group-item"><strong>Endereço:</strong>
                                                    @{{userData.endereco}}</li>

                                                <li class="list-group-item"><strong>Número:</strong>
                                                    @{{userData.numero}}</li>

                                                <li class="list-group-item"><strong>Complemento:</strong>
                                                    @{{userData.complemento}}</li>

                                                <li class="list-group-item"><strong>Ponto de Referência:</strong>
                                                    @{{userData.ponto_referencia}}</li>

                                                <li class="list-group-item"><strong>Quantos Funcionários:</strong>
                                                    @{{userData.quantos_funcionario}}</li>

                                                <li class="list-group-item"><strong>Data de Nascimento:</strong>
                                                    @{{userData.data_nascimento | date: "dd/MM/yyyy"}}</li>

                                                <li class="list-group-item"><strong>Sexo:</strong> @{{userData.sexo}}
                                                </li>

                                                <li class="list-group-item"><strong>Telefone Fixo:</strong>
                                                    @{{userData.telefone_fixo}}</li>

                                                <li class="list-group-item"><strong>Telefone Celular:</strong>
                                                    @{{userData.telefone_celular}}</li>

                                                <li class="list-group-item"><strong>Cep de Entrega:</strong>
                                                    @{{userData.cep_entrega}}</li>

                                                <li class="list-group-item"><strong>Estado de Entrega:</strong>
                                                    @{{userData.estado_entrega}}</li>

                                                <li class="list-group-item"><strong>Cidade de Entrega:</strong>
                                                    @{{userData.cidade_entrega}}</li>

                                                <li class="list-group-item"><strong>Bairro de Entrega:</strong>
                                                    @{{userData.bairro_entrega}}</li>

                                                <li class="list-group-item"><strong>Endereço de Entrega:</strong>
                                                    @{{userData.endereco_entrega}}</li>

                                                <li class="list-group-item"><strong>Número de Entrega:</strong>
                                                    @{{userData.numero_entrega}}</li>

                                                <li class="list-group-item"><strong>Complemento de Entrega:</strong>
                                                    @{{userData.complemento_entrega}}</li>

                                                <li class="list-group-item"><strong>Ponto de Referência de
                                                        Entrega:</strong> @{{userData.ponto_referencia_entrega}}</li>

                                                <li class="list-group-item"><strong>Quantos Funcionários de
                                                        Entrega:</strong> @{{userData.quantos_funcionario_entrega}}</li>

                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                <!-- Accordion card -->


                            </div>
                            <!-- Accordion wrapper -->





                        </div>

                    </div>
                    <!-- Card -->

                </div>
                <!-- Second column -->

            </div>
            <!-- First row -->

        </section>
        <!-- Section: Customers -->

    </div>
</main>
<!-- Main layout -->
@endsection
