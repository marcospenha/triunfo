@extends('layouts.app')

@section('content')

<!-- Main Layout -->
<main>

    <div class="container-fluid mb-5">

                        @if (session('captcha_erro'))
                    <div class="alert alert-danger" role="alert">
                     Selecione a caixa de captcha!
                    </div>
                @endif


        <!-- Grid row -->
        <div class="row" ng-controller="cadastroController">

            <!-- Grid column -->
            <div class="col-md-12">

                <div class="container">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('cadastrar') }}" name="formRegister">
                        @csrf

                        <!-- Section: Contact v.3 -->
                        <section class="contact-section my-5">


                            <!-- Form with header -->
                            <div class="card">

                                <!-- Grid row -->
                                <div class="row">

                                    <!-- Grid column -->
                                    <div class="col-lg-12">

                                        <div class="card-body form">

                                            <!-- Header -->
                                            <h3 class="mt-4 text-center">Meu Cadastro</h3>

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="cnpj" mask="00.000.000/0000-00"
                                                            ng-model="form.cnpj" type="text" id="cnpj"
                                                            class="form-control" ng-model required ui-br-cnpj-mask oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="cnpj" class="">CNPJ *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="email" ng-model="form.email" type="email"
                                                            id="email" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="email" class="">Seu email *</label>
                                            
                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="password" type="password"
                                                            ng-model="form.password" id="password"
                                                            class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="password" class="">Senha *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="password-confirmar" type="password"
                                                            ng-model="form.password_confirmar"
                                                            id="password-confirmar" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="password-confirmar" class="">Confirmar Senha
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                        </div>

                                    </div>
                                    <!-- Grid column -->

                                </div>
                                <!-- Grid row -->

                            </div>
                            <!-- Form with header -->

                        </section>
                        <!-- Section: Contact v.3 -->

                        <!-- Section: Contact v.3 -->
                        <section class="contact-section my-5">

                            <!-- Form with header -->
                            <div class="card">

                                <!-- Grid row -->
                                <div class="row">

                                    <!-- Grid column -->
                                    <div class="col-lg-12">

                                        <div class="card-body form">

                                            <!-- Header -->
                                            <h3 class="mt-4 text-center">Dados da Empresa</h3>

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="razao_social" ng-model="form.razao_social"
                                                            type="text" id="razao_social" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="razao_social" class="">Razão Social *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="inscricao_estadual"
                                                            ng-model="form.inscricao_estadual" type="text"
                                                            id="inscricao_estadual" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="inscricao_estadual" class="">Inscrição Estadual
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="nome_fantasia"
                                                            ng-model="form.nome_fantasia" type="text"
                                                            id="nome_fantasia" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="nome_fantasia" class="">Nome Fantasia
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-6">

                                                    <div class="md-form mb-0">

                                                        <input name="ramo_atividade" ng-model="form.ramo_atividade"
                                                            type="text" id="ramo_atividade" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="ramo_atividade" class="">Ramo de Atividade *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="cep" ng-model="form.cep"
                                                            ng-change="getEndereco()" type="text" id="cep"
                                                            class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="cep" class="">CEP *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="estado" ng-model="form.estado" type="text"
                                                            id="estado" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="estado" class=""
                                                            ng-class="{'active': form.estado !== undefined}">Estado
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="cidade" ng-model="form.cidade" type="text"
                                                            id="cidade" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="cidade" class=""
                                                            ng-class="{'active': form.cidade !== undefined}">Cidade
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="bairro" ng-model="form.bairro" type="text"
                                                            id="bairro" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="bairro" class=""
                                                            ng-class="{'active': form.bairro !== undefined}">Bairro
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="endereco" ng-model="form.endereco" type="text"
                                                            id="endereco" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="endereco" class=""
                                                            ng-class="{'active': form.endereco !== undefined}">Endereço
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="numero" ng-model="form.numero" type="text"
                                                            id="numero" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="numero" class="">Número *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="complemento" ng-model="form.complemento"
                                                            type="text" id="complemento" class="form-control">

                                                        <label for="complemento" class=""
                                                            ng-class="{'active': form.complemento !== undefined}">Complemento</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="ponto_referencia"
                                                            ng-model="form.ponto_referencia" type="text"
                                                            id="ponto_referencia" class="form-control">

                                                        <label for="ponto_referencia" class="">Ponto de
                                                            Referência</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="quantos_funcionario"
                                                            ng-model="form.quantos_funcionario" type="text"
                                                            id="quantos_funcionario" class="form-control">

                                                        <label for="quantos_funcionario" class="">Quantos
                                                            Funcionários</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->


                                        </div>

                                    </div>
                                    <!-- Grid column -->

                                </div>
                                <!-- Grid row -->

                            </div>
                            <!-- Form with header -->

                        </section>
                        <!-- Section: Contact v.3 -->

                        <!-- Section: Contact v.3 -->
                        <section class="contact-section my-5">

                            <!-- Form with header -->
                            <div class="card">

                                <!-- Grid row -->
                                <div class="row">

                                    <!-- Grid column -->
                                    <div class="col-lg-12">

                                        <div class="card-body form">

                                            <!-- Header -->
                                            <h3 class="mt-4 text-center">Dados do Comprador</h3>

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="nome_completo" ng-model="form.nome_completo"
                                                            type="text" id="nome_completo" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="nome_completo" class="">Nome Completo *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="data_nascimento" ng-model="form.data_nascimento"
                                                            type="date" id="data_nascimento" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="data_nascimento" class="">Data de Nascimento
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">
                                                        <label for="sexo" class="active">Sexo *</label>
                                                        <div class="form-check form-check-inline">
                                                            <input type="radio" class="form-check-input" id="sexo1"
                                                                name="sexo" value="Feminino" ng-model="form.sexo">
                                                            <label class="form-check-label" for="sexo1">Feminino</label>
                                                        </div>

                                                        <!-- Material inline 2 -->
                                                        <div class="form-check form-check-inline">
                                                            <input type="radio" class="form-check-input" id="sexo2"
                                                                name="sexo" value="Masculino" ng-model="form.sexo">
                                                            <label class="form-check-label"
                                                                for="sexo2">Masculino</label>
                                                        </div>

                                                        <!--Blue select-->

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="profissao" ng-model="form.profissao" type="text"
                                                            id="profissao" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="profissao" class="">Cargo *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="telefone_fixo" ng-model="form.telefone_fixo" ui-br-phone-number type="text"
                                                            id="telefone_fixo" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="telefone_fixo" class="">Telefone Fixo *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="telefone_celular"
                                                        ng-model="form.telefone_celular" ui-br-phone-number type="text"
                                                            id="telefone_celular" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="telefone_celular" class="">Telefone Celular
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                        </div>

                                    </div>
                                    <!-- Grid column -->

                                </div>
                                <!-- Grid row -->

                            </div>
                            <!-- Form with header -->

                        </section>
                        <!-- Section: Contact v.3 -->

                        <!-- Section: Contact v.3 -->
                        <section class="contact-section my-5">

                            <!-- Form with header -->
                            <div class="card">

                                <!-- Grid row -->
                                <div class="row">

                                    <!-- Grid column -->
                                    <div class="col-lg-12">

                                        <div class="card-body form">

                                            <!-- Header -->
                                            <h3 class="mt-4 text-center">Dados da Entrega</h3>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="md-form mb-0">
                                                        <input type="checkbox" ng-model="copiarEnderecoModel"
                                                        class="form-check-input" id="copiarEnderecoModel" ng-click="copiarEndereco()">
                                                        <label class="form-check-label" for="copiarEnderecoModel">
                                                            Usar endereço da empresa
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="cep_entrega" ng-model="form.cep_entrega"
                                                            ng-change="getEnderecoEntrega()" type="text" id="cep_entrega"
                                                            class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="cep_entrega" ng-class="{'active': form.cep_entrega !== undefined}">CEP *</label>


                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="estado_entrega" ng-model="form.estado_entrega"
                                                            type="text" id="estado_entrega" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="estado_entrega" class=""
                                                            ng-class="{'active': form.estado_entrega !== undefined}">Estado
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="cidade_entrega" ng-model="form.cidade_entrega"
                                                            type="text" id="cidade_entrega" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="cidade_entrega" class=""
                                                            ng-class="{'active': form.cidade_entrega !== undefined}">Cidade
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="bairro_entrega" ng-model="form.bairro_entrega"
                                                            type="text" id="bairro_entrega" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="bairro_entrega" class=""
                                                            ng-class="{'active': form.bairro_entrega !== undefined}">Bairro
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="endereco_entrega"
                                                            ng-model="form.endereco_entrega" type="text"
                                                            id="endereco_entrega" class="form-control" required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="endereco_entrega" class=""
                                                            ng-class="{'active': form.endereco_entrega !== undefined}">Endereço
                                                            *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="numero_entrega" ng-model="form.numero_entrega"
                                                            type="text" id="numero_entrega" class="form-control"
                                                            required oninvalid="setCustomValidity('Campo Obrigatório!')" 
                                                            onchange="try{setCustomValidity('')}catch(e){}">

                                                        <label for="numero_entrega" ng-class="{'active': form.numero_entrega !== undefined}" class="">Número *</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="complemento_entrega"
                                                            ng-model="form.complemento_entrega" type="text"
                                                            id="complemento_entrega" class="form-control">

                                                        <label for="complemento_entrega" class=""
                                                            ng-class="{'active': form.complemento_entrega !== undefined}">Complemento</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                                <!-- Grid column -->
                                                <div class="col-md-4">

                                                    <div class="md-form mb-0">

                                                        <input name="ponto_referencia_entrega"
                                                            ng-model="form.ponto_referencia_entrega" type="text"
                                                            id="ponto_referencia_entrega" class="form-control">

                                                        <label for="ponto_referencia_entrega" class="">Ponto de
                                                            Referência</label>

                                                    </div>

                                                </div>
                                                <!-- Grid column -->

                                            </div>
                                            <!-- Grid row -->


                                        </div>

                                    </div>
                                    <!-- Grid column -->

                                </div>
                                <!-- Grid row -->

                            </div>
                            <!-- Form with header -->

                        </section>
                        <!-- Section: Contact v.3 -->

                        <!-- Section: Contact v.3 -->
                        <section class="contact-section my-5">

                            <!-- Form with header -->
                            <div class="card">

                                <!-- Grid row -->
                                <div class="row">

                                    <!-- Grid column -->
                                    <div class="col-lg-12">

                                        <div class="card-body form">

                                            <!-- Grid row -->
                                            <div class="row">

                                                <!-- Grid column -->
                                                <div class="col-md-12">
                                                    <h5 class="mt-4 text-center">TERMOS E CONDIÇÕES</h5>
                                                </div>

                                                <div class="col-md-12">
                                                    <p>
                                                        <meta charset="utf-8">
                                                    </p>
                                                    <h3 class="text-center"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; font-family: Verdana, Arial, sans-serif; font-weight: 500; line-height: 1.1; color: rgb(102, 102, 102); margin-top: 20px; margin-bottom: 10px; font-size: 18px; text-align: center; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box;"><span
                                                                style="box-sizing: border-box; font-weight: 700;"><u>Acordo de Fornecimento – Loja Triunfo Online
                                                                    </u></span></span></h3>
                                                    <p>
                                                        <br>
                                                    </p>
                                                    <p class="text-justify"
                                                    style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Formato
                                                        de Atendimento:</span> As Solicitações de Cotações serão geradas pelo Cliente no Portal da Loja Triunfo Online através de Login e Senha e serão respondidas em até 24 horas.
                                                        Após aprovadas, as Cotações serão transformadas em Pedidos que serão faturados e entregues no Endereço de Entrega, pré-cadastrado, pelo Cliente.
                                                        Os Prazos de Entrega variam com a localização e deverão ser  informados no momento do Fechamento do Pedido.
                                                    </p>
                                                    <p class="text-justify"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Prazo de
                                                            Pagamento:</span> Conforme escolha do Cliente na Realização do Pedido e critérios da Loja Triunfo Online.
                                                        </p>
                                                    <p class="text-justify"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Forma de Pagamento:</span> 
                                                        Conforme escolha do Cliente na Realização do Pedido e critérios da Loja Triunfo Online.</p>
                                                    <p class="text-justify"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Condição
                                                            de Faturamento:</span> Conforme escolha do Cliente na Realização do Pedido e critérios da Loja Triunfo Online.</p>
                                                    <p class="text-justify"
                                                    style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Prazo de Entrega:</span> 
                                                        As entregas serão realizadas em até 24 horas respeitando, sempre, o horário comercial, entre 8h e 17h.</p>        
                                                    <p class="text-justify"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Horário de Recebimento das Cotações e Pedidos:</span>
                                                        <br style="box-sizing: border-box;">• 24 horas.
                                                    </p>
                                                    <p class="text-justify"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Frete
                                                            dos Pedidos:</span>
                                                        <br style="box-sizing: border-box;">• Isento para os Pedidos
                                                        acima de: R$ 200,00
                                                        <br style="box-sizing: border-box;">• Pedido mínimo para
                                                        faturamento: R$ 200,00
                                                        <br style="box-sizing: border-box;">• Pedidos emergenciais que necessitem contratação de frete especial serão cobrados com taxa extra, orçados pontualmente a cada situação, devendo antes serem aprovados pelo Cliente.
                                                    </p>
                                                    <p class="text-justify"
                                                        style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial; box-sizing: border-box; margin: 0px 0px 10px; text-align: justify; color: rgb(102, 102, 102); font-family: Verdana, Arial, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);">
                                                        <span style="box-sizing: border-box; font-weight: 700;">Validade das Cotações:</span>
                                                        <br style="box-sizing: border-box;">• 48 horas.
                                                    </p>
                                                </div>

                                                <!-- Grid column -->
                                                <!-- Material unchecked -->


                                            </div>
                                            <!-- Grid row -->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="checkbox" ng-model="checkboxModel"
                                                        class="form-check-input" id="materialUnchecked" >
                                                    <label class="form-check-label" for="materialUnchecked">Eu aceito os
                                                        Termos e Condições* </label>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                    <!-- Grid column -->

                                </div>
                                <!-- Grid row -->

                            </div>
                            <!-- Form with header -->

                        </section>
                        <!-- Section: Contact v.3 -->

                         <div class="row">
                          <div class="col-md-12">
                              <div class="g-recaptcha" data-sitekey="6LeHj-AZAAAAACsvBFbRRx7iBl2ZqjsJvA-3OJNs"></div>
                          </div>
                      </div>
                          </br>

                       
                        <!-- Grid row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <a href="/" class="btn btn-outline-info btn-md mb-4 waves-effect waves-light">Cancelar</a>
                                    <button class="btn btn-outline-info btn-md mb-4 waves-effect waves-light"
                                        type="submit" value="LOGIN"
                                        ng-disabled="!checkboxModel">Cadastrar</button>
                                </div>
                            </div>

                        </div>
                    </form>

                </div>

            </div>


            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>

</main>
<!-- Main Layout -->

@endsection
