@extends('layouts.app')

@section('content')

<!-- Main Layout -->
<main>

<div class="container-fluid mb-5">

    <!-- Grid row -->
    <div class="row">

    <!-- Grid column -->
    <div class="col-md-12">

        <div class="container">

            <!-- Section: Contact v.3 -->
            <section class="contact-section my-5">

            <!-- Form with header -->
            <div class="">

                <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-5">

                    <!-- Form with header -->
                    <div class="card wow fadeIn" data-wow-delay="0.3s">
                    <div class="card-body">

                        <!-- Header -->
                        <div class="form-header green-gradient">
                        <h3 class="font-weight-500 my-2 py-1"><i class="fas fa-user"></i> Entrar</h3>
                        </div>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                        <div class="md-form">
                            <i class="fas fa-envelope prefix white-text"></i>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <label for="orangeForm-email">Seu email</label>
                        </div>

                        <div class="md-form">
                            <i class="fas fa-lock prefix white-text"></i>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            <label for="orangeForm-pass">Sua Senha</label>
                        </div>
                        <div class="d-flex justify-content-around pull-right">
                            <div>
                                <!-- Forgot password -->
                                <a href="/password/reset">Esqueceu a senha?</a>
                            </div>
                        </div>
                        <div class="text-center">
                            <button class="btn green-gradient btn-lg" [disabled]="!loginForm.valid">Entrar</button>
                        </div>

                        </form>
                    </div>
                    </div>
                    <!-- Form with header -->

                </div>
                </div>

            </div>
            <!-- Form with header -->

            </section>
            <!-- Section: Contact v.3 -->

        </div>

    </div>


    <!-- Grid column -->

    </div>
    <!-- Grid row -->

</div>

</main>
<!-- Main Layout -->

@endsection
