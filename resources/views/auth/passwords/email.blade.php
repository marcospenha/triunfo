@extends('layouts.app')

@section('content')

<!-- Main Layout -->
<main>

<div class="container-fluid mb-5">

    <!-- Grid row -->
    <div class="row">

    <!-- Grid column -->
    <div class="col-md-12">

        <div class="container">

            <!-- Section: Contact v.3 -->
            <section class="contact-section my-5">

            <!-- Form with header -->
            <div class="">

                <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-5">

                    <!-- Form with header -->
                    <div class="card wow fadeIn" data-wow-delay="0.3s">
                    <div class="card-body">

                        <!-- Header -->
                        <div class="form-header green-gradient">
                        <h3 class="font-weight-500 my-2 py-1"><i class="fas fa-user"></i> Redefinir Senha</h3>
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                Um link para redefinir senha foi enviado para o seu endereço de e-mail.
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                        <div class="md-form">
                            <i class="fas fa-envelope prefix white-text"></i>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <label for="orangeForm-email">Seu email</label>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Enviar link de redefinição de senha') }}
                            </button>
                        </div>

                        </form>
                    </div>
                    </div>
                    <!-- Form with header -->

                </div>
                </div>

            </div>
            <!-- Form with header -->

            </section>
            <!-- Section: Contact v.3 -->

        </div>

    </div>


    <!-- Grid column -->

    </div>
    <!-- Grid row -->

</div>

</main>
<!-- Main Layout -->

@endsection
