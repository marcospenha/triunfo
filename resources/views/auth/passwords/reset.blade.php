@extends('layouts.app')

@section('content')

<div class="container-fluid mb-5">

    <!-- Grid row -->
    <div class="row">

    <!-- Grid column -->
    <div class="col-md-12">

        <div class="container">

            <!-- Section: Contact v.3 -->
            <section class="contact-section my-5">

            <!-- Form with header -->
            <div class="">

                <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-10 col-sm-12 mx-auto mt-5">

                    <!-- Form with header -->
                    <div class="card wow fadeIn" data-wow-delay="0.3s">
                    <div class="card-body">

                        <!-- Header -->
                        <div class="form-header green-gradient">
                        <h3 class="font-weight-500 my-2 py-1"><i class="fas fa-user"></i> Redefinir Senha</h3>
                        </div>
                    <form method="POST" action="{{ route('password.editar') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Seu Email</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required readonly>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Senha</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Confirmar Senha</label>

                            <div class="col-md-8">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Redefinir Senha
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Form with header -->

        </div>
        </div>

    </div>
    <!-- Form with header -->

    </section>
    <!-- Section: Contact v.3 -->

</div>

</div>


<!-- Grid column -->

</div>
<!-- Grid row -->

</div>

</main>
<!-- Main Layout -->

@endsection
