<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <title>Loja Triunfo</title>
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="/assets/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/assets/css/mdb.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body class="homepage-v1 hidden-sn white-skin animated" ng-app="triungoApp" ng-cloak>

    <!-- Navigation -->
    
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg  navbar-light scrolling-navbar white">

            <div class="container">

                <a class="navbar-brand font-weight-bold" href="/">

                    <img src="/assets/img/logo.jpeg"
                     alt="Triunfo Distribuidora" style="
                     width: 62px;
                     height: 62px;
                 ">

                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4"
                    aria-expanded="false" aria-label="Toggle navigation">

                    <span class="navbar-toggler-icon"></span>

                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent-4">

                    <ul class="navbar-nav ml-auto">


                        <li class="nav-item">

                            <a class="nav-link waves-effect waves-light dark-grey-text font-weight-bold"
                                href="/">

                                Ínicio

                            </a>

                        </li>

                        <li class="nav-item">

                            <a class="nav-link waves-effect waves-light dark-grey-text font-weight-bold"
                                href="/contato">

                                Contato

                                <span class="sr-only">(current)</span>

                            </a>

                        </li>


                       

                    </ul>

                    <!-- Collapsible content -->
                    <div class="navbar-collapse" id="navbarSupportedContent" ng-controller="inicioController">

                        <form ng-submit="submitBusca()" class="form-inline ml-auto">
                            <div class="md-form my-0">
                                <input class="form-control" type="text" placeholder="Buscar" aria-label="Buscar" name="busca" ng-model="busca">
                            </div>
                            <button href="#!" class="btn mr-0 btn-primary btn-rounded clearfix d-md-inline-block waves-effect waves-light" type="submit">Buscar</button>
                        </form>
                              
                    </div>
                    <ul class="navbar-nav ml-auto">
                        @guest
                        <li class="nav-item">

                            <a class="nav-link waves-effect waves-light dark-grey-text font-weight-bold" href="/login">

                                Cotação Online

                                <span class="sr-only">(current)</span>

                            </a>

                        </li>
                        @if (Route::has('register'))
                        <li class="nav-item">

                            <a class="nav-link waves-effect waves-light dark-grey-text font-weight-bold"
                                href="/register">

                                Cadastrar

                                <span class="sr-only">(current)</span>

                            </a>

                        </li>
                        @endif
                        @else
                        <li class="nav-item">

                            <a class="nav-link waves-effect waves-light dark-grey-text font-weight-bold"
                                href="/cotacao">

                                <i class="fas fa-cart-plus blue-text" aria-hidden="true"></i> Cotação

                            </a>

                        </li>

                        

                        <li class="nav-item dropdown ml-3">

                            <a class="nav-link dropdown-toggle waves-effect waves-light dark-grey-text font-weight-bold"
                                id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">

                                <i class="fas fa-user blue-text"></i> {{ Auth::user()->razao_social }} </a>

                            <div class="dropdown-menu dropdown-menu-right dropdown-cyan"
                                aria-labelledby="navbarDropdownMenuLink-4">

                                <a class="dropdown-item waves-effect waves-light" href="{{ route('perfil') }}">Minha Conta</a>

                                <a class="dropdown-item waves-effect waves-light" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Sair
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>

                            </div>

                        </li>

                        @endguest

                    </ul> 
                </div>

            </div>           

        </nav>
        <!-- Navbar -->

    </header>
    <!-- Navigation -->

    <main class="py-4">
        @yield('content')
    </main>

    <!-- Footer -->
    <footer class="page-footer text-center text-md-left stylish-color-dark pt-0"
        style="background-color: #26671b!important;">

        <div style="background-color: #06522a;">

            <div class="container">

                <!-- Grid row -->
                <div class="row py-4 d-flex align-items-center">

                    <!-- Grid column -->
                    <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">

                        <h6 class="mb-0 white-text">Nos acompanhe nas redes sociais!</h6>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-6 col-lg-7 text-center text-md-right">

                        <!-- Facebook -->
                        <a class="fb-ic ml-0 px-2" href="https://www.facebook.com/triunfopapeis/" target="_blank">

                            <i class="fab fa-facebook-f white-text"> </i>

                        </a>

                        <!-- Linkedin -->
                        <a class="li-ic px-2" href="https://www.linkedin.com/company/triunfo-papeis/" target="_blank">

                            <i class="fab fa-linkedin-in white-text"> </i>

                        </a>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

            </div>

        </div>

        <!-- Footer Links -->
        <div class="container mt-5 mb-4 text-center text-md-left">

            <div class="row mt-3">

                <!-- First column -->
                <div class="col-md-3 col-lg-4 col-xl-3 mb-4">

                    <h6 class="text-uppercase font-weight-bold">

                        <a href="http://www.triunfopapeis.com.br/" target="_blank"><strong>Loja Triunfo</strong></a>

                    </h6>

                    <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

                </div>
                <!-- First column -->

                <!-- Fourth column -->
                <div class="col-md-4 col-lg-3 col-xl-3">

                    <h6 class="text-uppercase font-weight-bold">

                        <strong>Contato</strong>

                    </h6>

                    <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

                    <p>

                        <i class="fas fa-envelope mr-3"></i> vendas@triunfopapeis.com.br</p>

                    <p>

                        <i class="fas fa-phone mr-3"></i> (98) 99112-7777</p>

                    <p>

                        <i class="fab fa-whatsapp mr-3"></i> (98) 99112-7777</p>
    

                </div>
                <!-- Fourth column -->

            </div>

        </div>
        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright py-3 text-center">

            <div class="container-fluid">

                © 2020 Copyright: <a href="http://eixopositivo.com.br/" target="_blank"> Eixo Positivo </a>

            </div>

        </div>
        <!-- Copyright -->

    </footer>
    <script type="text/javascript" src="/angular-1.7.9/angular.min.js"></script>
    <script type="text/javascript" src="/angular-slugify/angular-slugify.js"></script>

    
    <script type="text/javascript" src="/js/angular-app.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="/assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="/assets/js/bootstrap.min.js">
    </script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="/assets/js/mdb.min.js"></script>

    <script type="text/javascript" src="/bower_components/angular-input-masks/angular-input-masks.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-input-masks/angular-input-masks-standalone.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-input-masks/angular-input-masks-dependencies.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-input-masks/angular-input-masks.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-input-masks/angular-input-masks.br.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular/angular-locale_pt-br.js"></script>
    <script>
        // SideNav Initialization
        $(".button-collapse").sideNav();

        // Material Select Initialization
        $(document).ready(function () {
            $('.mdb-select').materialSelect();

        });

    </script>
</body>

</html>
