@extends('layouts.app')

@section('content')

  <!-- Main Layout -->
  <main>

    <div class="container-fluid mb-5">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-12">

            <div class="container">

              <!-- Section: Contact v.3 -->
              <section class="contact-section my-5">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                      Sua mensagem foi enviada com sucesso, entraremos em contato o mais rápido possível.
                    </div>
                @endif
                @if (session('captcha_erro'))
                    <div class="alert alert-danger" role="alert">
                     Selecione a caixa de captcha!
                    </div>
                @endif
                <!-- Form with header -->
                <div class="card">

                  <!-- Grid row -->
                  <div class="row">
                    
                    <!-- Grid column -->
                    <div class="col-lg-8">

                      <div class="card-body form">

                        <!-- Header -->
                        <h3 class="mt-4"><i class="fas fa-envelope pr-2"></i>Fale Conosco:</h3>

                        <form id="my_form" method="post" action="{{ route('enviarEmail') }}">
                          @csrf
                            <!-- Grid row -->
                          <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-6">

                              <div class="md-form mb-0">

                                <input type="text" id="form-contact-name" class="form-control" name="nome">

                                <label for="form-contact-name" class="">Seu nome</label>

                              </div>

                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-md-6">

                              <div class="md-form mb-0">

                                <input type="text" id="form-contact-email" class="form-control" name="email">

                                <label for="form-contact-email" class="">Seu email</label>

                              </div>

                            </div>
                            <!-- Grid column -->

                          </div>
                          <!-- Grid row -->

                          <!-- Grid row -->
                          <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-6">

                              <div class="md-form mb-0">

                                <input type="text" id="form-contact-phone" class="form-control" name="telefone">

                                <label for="form-contact-phone" class="">Seu telefone</label>

                              </div>

                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-md-6">

                              <div class="md-form mb-0">

                                <input type="text" id="form-contact-company" class="form-control" name="assunto">

                                <label for="form-contact-company" class="">Assunto</label>

                              </div>
                            
                            </div>
                            <!-- Grid column -->

                          </div>
                          <!-- Grid row -->

                          <!-- Grid row -->
                          <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-12">

                              <div class="md-form mb-0">

                                <textarea type="text" id="form-contact-message" class="form-control md-textarea" name="mensagem"
                                  rows="3"></textarea>

                                <label for="form-contact-message">Sua mensagem</label>
 

          
                               

                              </div>

                            </div>
                            <!-- Grid column -->

                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="g-recaptcha" data-sitekey="6LeHj-AZAAAAACsvBFbRRx7iBl2ZqjsJvA-3OJNs"></div>
                              </div>
                          </div>
                          </br>
                              <div class="row">
                              <div class="col-md-12">
                                 <input type="submit" value="Enviar" class="btn mr-0 btn-primary btn-rounded clearfix d-md-inline-block waves-effect waves-light">
                              </div>
                          </div>
                          
                          <!-- Grid row -->
                        </form>

                      </div>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-4">

                      <div class="card-body contact text-center h-100 white-text" style="background-color: #2d801f;">

                        <h3 class="my-4 pb-2">Informações de Contato</h3>

                        <ul class="text-lg-left list-unstyled ml-4">

                          <li>

                            <p><i class="fas fa-phone pr-2 mb-4" style="
                              color: #fff;"></i>(98) 3131-7777</p>

                          </li>

                          <li>

                            <p><i class="fas fa-envelope pr-2" style="
                              color: #fff;"></i>vendas@triunfopapeis.com.br</p>
  
                          </li>

                        </ul>

                        <hr class="hr-light my-4">

                      </div>

                    </div>
                    <!-- Grid column -->

                  </div>
                  <!-- Grid row -->

                </div>
                <!-- Form with header -->

              </section>
              <!-- Section: Contact v.3 -->

            </div>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>

  </main>
  <!-- Main Layout -->
  
@endsection