@extends('layouts.app')

@section('content')

<div ng-controller="inicioController">
<!-- Intro -->
  <section>

    <!-- Carousel Wrapper -->
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
  
      <!-- Indicators -->
      <ol class="carousel-indicators">
  
        <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
  
        <li ng-if="slide_2!==null" data-target="#carousel-example-1z" data-slide-to="1"></li>
  
        <li ng-if="slide_3!==null" data-target="#carousel-example-1z" data-slide-to="2"></li>
  
      </ol>
      <!-- Indicators -->
  
      <!-- Slides -->
      <div class="carousel-inner" role="listbox">
  
        <!-- First slide -->
        <div class="carousel-item active">
  
          <div class="view h-100">
  
          <img class="d-block h-100 w-lg-100 filtro" src="/@{{slides.slide_1}}"
              alt="First slide">
  
            <div class="mask">
  
              <!-- Caption -->
              <div class="full-bg-img flex-center white-text">
  
                <ul class="animated fadeIn col-10 list-unstyled">
  
                  <li>
  
                    <p class="h1 mb-4 mt-5">
  
                      <strong>@{{slides.slogan_1}}</strong>
  
                    </p>
  
                  </li>
  
                  <li>
 
                    <h5 class="h5-responsive dark-grey-text font-weight-bold mb-5">@{{slides.subslogan_1}}</h5> 
  
                  </li>

                </ul>
  
              </div>
              <!-- Caption -->
  
            </div>
  
          </div>
  
        </div>
        <!-- First slide -->
  
        <!-- Second slide -->
        <div class="carousel-item h-100">
  
          <div class="view h-100">
  
            <img class="d-block h-100 w-lg-100 filtro" src="/@{{slides.slide_2}}"
              alt="Second slide">
  
            <div class="mask">
  
              <!-- Caption -->
              <div class="full-bg-img flex-center white-text">
  
                <ul class="animated fadeIn col-10 list-unstyled">
  
                  <li>
  
                    <p class="h1 mb-4">
  
                      <strong>@{{slides.slogan_2}}</strong>
  
                    </p>
  
                  </li>
  
                  <li>
  
                    <h5 class="h5-responsive font-weight-bold mb-5">@{{slides.subslogan_2}}</h5>
  
                  </li>
  
                </ul>
  
              </div>
              <!-- Caption -->
  
            </div>
  
          </div>
  
        </div>
        <!-- Second slide -->
  
        <!-- Third slide -->
        <div class="carousel-item">
  
          <div class="view h-100">
  
            <img class="d-block h-100 w-lg-100 filtro" src="/@{{slides.slide_3}}"
              alt="Third slide">
  
            <div class="mask">
  
              <!-- Caption -->
              <div class="full-bg-img flex-center white-text">
  
                <ul class="animated fadeIn col-md-10 text-center text-md-right list-unstyled">
  
                  <li>
  
                    <p class="h1 white-text mb-4 mt-5 pr-lg-5">
  
                      <strong>@{{slides.slogan_3}}</strong>
  
                    </p>
  
                  </li>
  
                  <li>
  
                    <h5 class="h5-responsive white-text font-weight-bold mb-5 pr-lg-5">@{{slides.subslogan_3}}</h5>
  
                  </li>
  
                </ul>
  
              </div>
              <!-- Caption -->
  
            </div>
  
          </div>
  
        </div>
        <!-- Third slide -->
  
      </div>
      <!-- Slides -->
  
      <!-- Controls -->
      <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
  
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  
        <span class="sr-only">Previous</span>
  
      </a>
  
      <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
  
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
  
        <span class="sr-only">Next</span>
  
      </a>
      <!-- Controls -->
  
    </div>
  
    <!-- Carousel Wrapper -->
    <div class="container-fluid mx-0 px-0">
  
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-dark primary-color mb-5" style="background-color: #2e901d!important;">
  
        <div class="container">
  
          <!-- Collapse button -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
            aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
  
            <span class="navbar-toggler-icon"></span>
  
          </button>
  
        </div>
  
      </nav>
      <!-- Navbar -->
  
    </div>
  
  </section>
  <!-- Intro -->
  
  <!-- Main Container -->
  <div class="container">
  
    <!-- Grid row -->
    <div class="row pt-4">
  
      <!-- Content -->
      <div class="col-lg-12">
  
        <!-- Section: Advertising -->
        <section>
  
          <!-- Grid row -->
          <div class="row" ng-show="itemDestaque">
            
            <!-- Grid column -->
            <div class="col-sm-12">
  
              <!-- Image -->
              <div class="view  z-depth-1">
                
                  <img src="@{{itemDestaque.foto}}" class="img-fluid" alt="sample image" style="height: 300px;width: 300px;padding-top: 35px;"> 

                <div class="mask rgba-stylish-slight">
  
                  <div class="dark-grey-text text-right pt-lg-5 pt-md-1 mr-5 pr-md-4 pr-0">
  
                    <div>
  
                      <h2 class="card-title font-weight-bold pt-md-3 pt-1">
  
                        <strong>@{{itemDestaque.titulo}}
  
                        </strong>
  
                      </h2>
  
                      <p class="pb-lg-3 pb-md-1 clearfix d-none d-md-block">@{{itemDestaque.subtitulo}}</p>
  
                      <a href="@{{itemDestaque.link}}" class="btn mr-0 btn-primary btn-rounded clearfix mobile-button d-md-inline-block">Ver Mais</a>
  
                    </div>
  
                  </div>
  
                </div>
  
              </div>
              <!-- Image -->
  
            </div>
            <!-- Grid column -->
  
          </div>
          <!-- Grid row -->
  
        </section>
        <!-- Section: Advertising -->
  
        <!-- Section: product list -->
        <section class="mb-5">
         
      <!-- Navbar -->
      <div class="row pt-4">

        <!-- Sidebar -->
        <div class="col-sm-3">
    
          <div class="">
    
            <!-- Grid row -->
            <div class="row">
    
              <!-- Filter by category -->
              <div class="col-md-6 col-lg-12 mb-5" style="padding-top: 95px;">
    
                <h4 class="mdb-main-label">Categorias</h4>
    
                  <div class="divider"></div>
    
                      <div ng-repeat="categoria in categoriasLista">
                        <div class="form-group">
    
                          <a class="menu-item waves-effect waves-light green-a" ng-click="getCategoriaID(categoria.id)" href="/categoria/@{{categoria.nome | slugify}}">@{{categoria.nome}}</a>
    
                        </div>
                      </div>
                      <!-- Radio group -->
    
              </div>
    
              <!-- Filter by category -->
            </div>
            <!-- Grid row -->
    
          </div>
    
        </div>
        <!-- Sidebar -->
    
        <!-- Content -->
        <div class="col-sm-9">
    
          <!-- Filter Area -->
          <div class="row">
    
            <div class="col-md-4 mt-3">
    
              <h5 class="font-weight-bold dark-grey-text"><strong>@{{categoriasLista.nome}}</strong></h5>
              <!-- Sort by -->
            </div>
    
    
          </div>
          <!-- Filter Area -->
    
          <!-- Products Grid -->
          <section class="section pt-4">
    
            <!-- Grid row -->
            <div class="row">
    
              <!-- Grid column -->
              <div ng-repeat="produto in produtosLista" class="col-lg-4 col-md-12 mb-4">
    
              <!-- First row -->
              <div class="row mt-5 hoverable align-items-center">
  
                <div class="col-sm-6">
  
                  <a  href="/produto/@{{produto.id}}/@{{produto.nome | slugify}}">
  
                    <img src="/@{{produto.foto_1}}"
                      class="img-fluid">
  
                  </a>
  
                </div>
  
                <div class="col-sm-6">
  
                  <!-- Title -->
                  <a  href="/produto/@{{produto.id}}/@{{produto.nome | slugify}}">
  
                    <strong>@{{produto.nome}}</strong>
  
                  </a>
  
  
  
                  <!-- Price -->
                  <h6 class="h6-responsive font-weight-bold dark-grey-text">
  
                    <strong>@{{produto.codigo_produto}}</strong>
  
                  </h6>
  
                </div>
  
              </div>
              <!-- First row -->
    
              </div>
              <!-- Grid column -->
    
            </div>
            <!-- Grid row -->
    
    
          </section>
          <!-- Products Grid -->
    
        </div>
        <!-- Content -->
    
      </div>
    
  
        </section>
        <!-- Section: product list -->
  
        <!-- Section: Last items -->
        <section>
  
          <h4 class="font-weight-bold mt-4 dark-grey-text">
  
            <strong>NOVIDADES</strong>
  
          </h4>
  
          <hr class="mb-5">
  
          <!-- Grid row -->
          <div class="row">
  
            <!-- Grid column -->
            <div ng-repeat="produto in produtosRecentesLista" class="col-lg-3 col-sm-6 mb-4">
  
              <!-- Card -->
              <div class="card card-ecommerce" style="height: 400px;width: 250px;">
  
                <!-- Card image -->
                <div class="view overlay">
  
                  <img src="/@{{produto.foto_1}}" class="img-fluid"
                    alt="sample image">
  
                  <a  href="/produto/@{{produto.id}}/@{{produto.nome | slugify}}">
  
                    <div class="mask rgba-white-slight"></div>
  
                  </a>
  
                </div>
                <!-- Card image -->
  
                <!-- Card content -->
                <div class="card-body">
  
                  <!-- Category & Title -->
                  <h5 class="card-title mb-1">
  
                    <strong>
  
                      <a  href="/produto/@{{produto.id}}/@{{produto.nome | slugify}}" class="dark-grey-text">@{{produto.nome}}</a>
  
                    </strong>
  
                  </h5>
  
  
                  <!-- Card footer -->
                  <div class="card-footer pb-0">
  
                    <div class="row mb-0">
  
                      <span class="float-left">
  
                        <strong>@{{produto.codigo_produto}}</strong>
  
                      </span>
  
  
  
                    </div>
  
                  </div>
  
                </div>
                <!-- Card content -->
  
              </div>
              <!-- Card -->
  
            </div>
            <!-- Grid column -->
  
  
  
          </div>
          <!-- Grid row -->
  
          <!-- Grid row -->
  
          <!-- Grid row -->
  
        </section>
        <!-- Section: Last items -->
  
      </div>
      <!-- Content -->
  
    </div>
    <!-- Grid row -->
  
  </div>
  <!-- Main Container -->
</div>
@endsection