@extends('layouts.app')

@section('content')

<div ng-controller="categoriasController">

    <!-- Main Container -->
<div class="container mt-5 pt-3">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark primary-color mt-5 mb-5">
  
      <!-- Collapsible content -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent1">
  
        <!-- Links -->
        <ul class="navbar-nav mr-auto">
  
  
            <li class="nav-item dropdown mega-dropdown active">
  
              <a class="nav-link dropdown-toggle  no-caret" id="navbarDropdownMenuLink1" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Categorias</a>
  
              <div class="dropdown-menu mega-menu v-2 row z-depth-1 white" aria-labelledby="navbarDropdownMenuLink1">
  
                <div class="row mx-md-4 mx-1">
                  
                    <div ng-repeat="lista in categoriasListaTodas" class="col-md-6 col-xl-3 sub-menu my-xl-5 mt-5 mb-4">
  
                      <ul class="caret-style pl-0">
                        <div  ng-repeat="categoria in lista" >
                          <li class="">
  
                            <a class="menu-item waves-effect waves-light" ng-click="getCategoriaID(categoria.id)" href="/categoria/@{{categoria.nome | slugify}}">@{{categoria.nome}}</a>
  
                          </li>
                        </div>
  
  
                      </ul>
  
                    </div>
                  
  
                </div>
  
              </div>
  
            </li>
  
  
        </ul>
  
        <!-- Links -->

      </div>
      <!-- Collapsible content -->
  
    </nav>
    <div class="row">
      <div class="col-sm-12">
        *As imagens do nosso Catálogo Online são meramente ilustrativas e podem variar dependendo do lote disponível em nosso estoque. 
        Para esclarecer qualquer dúvida contate-nos através dos nossos canais de atendimento
      </div>
    </div>
    <!-- Navbar -->
    <div class="row pt-4">
      
      <!-- Sidebar -->
      <div class="col-lg-3">
  
        <div class="">
  
          <!-- Grid row -->
          <div class="row">
  
            <!-- Filter by category -->
            <div class="col-md-6 col-lg-12 mb-5">
  
              <h4 class="mdb-main-label">Subcategorias</h4>
  
                <div class="divider"></div>
                    <!-- Radio group -->
                    <div class="form-group ">
  
                      <input class="form-check-input" ng-click="setValue('')" name="group100" type="radio" id="radio100" checked>
  
                      <label for="radio100" class="form-check-label dark-grey-text">Todas</label>
  
                    </div>
  
                    <div ng-repeat="subcategoria in subcategoriasLista" >
                      <div class="form-group">
  
                        <input class="form-check-input" ng-click="setValue(subcategoria.nome)" name="group100" type="radio" id="@{{subcategoria.id}}" >
  
                        <label for="@{{subcategoria.id}}" class="form-check-label dark-grey-text">@{{subcategoria.nome}}</label>
  
                      </div>
                    </div>
                    <!-- Radio group -->
  
            </div>
  
            <!-- Filter by category -->
          </div>
          <!-- Grid row -->
  
        </div>
  
      </div>
      <!-- Sidebar -->
  
      <!-- Content -->
      <div class="col-lg-9">
  
        <!-- Filter Area -->
        <div class="row">
  
          <div class="col-md-4 mt-3">
  
            <h5 class="font-weight-bold dark-grey-text"><strong>@{{categoriasLista.nome}}</strong></h5>
            <!-- Sort by -->
          </div>
  
  
        </div>
        <!-- Filter Area -->
  
        <!-- Products Grid -->
        <section class="section pt-4">
  
          <!-- Grid row -->
          <div class="row">
  
            <!-- Grid column -->
            <div ng-repeat="produto in produtosListas | filter : {subcategoria : subcategoriaFiltro} | filter : buscar" class="col-lg-4 col-md-12 mb-4">
  
              <!-- Card -->
              <div class="card card-ecommerce" style="height: 400px;width: 250px;">
  
                <!-- Card image -->
                <div class="view overlay">
  
                  <img src="/@{{produto.foto_1}}" class="img-fluid"
                    alt="">
  
                  <a href="/produto/@{{produto.id}}/@{{produto.nome | slugify}}">
  
                    <div class="mask rgba-white-slight"></div>
  
                  </a>
  
                </div>
                <!-- Card image -->
  
                <!-- Card content -->
                <div class="card-body">
  
                  <!-- Category & Title -->
                  <h8 class="card-title mb-1"><strong><a href="/produto/@{{produto.id}}/@{{produto.nome | slugify}}" class="dark-grey-text">@{{produto.nome}}</a></strong></h8>
  
                  <!-- Card footer -->
                  <div class="card-footer pb-0">
  
                    <div class="row mb-0">
  
                      <span class="float-left"><strong>@{{produto.codigo_produto}}</strong></span>
  
                      <span class="float-right">
  
                        <a ng-click="inserirCarrinho(produto)" class="" data-toggle="tooltip" data-placement="top" title="Adicionar ao carrinho"><i
                            class="fas fa-shopping-cart ml-3"></i></a>
  
                      </span>
  
                    </div>
  
                  </div>
  
                </div>
                <!-- Card content -->
  
              </div>
              <!-- Card -->
  
            </div>
            <!-- Grid column -->
  
          </div>
          <!-- Grid row -->
  
  
        </section>
        <!-- Products Grid -->
  
      </div>
      <!-- Content -->
  
    </div>
  
  </div>
  <!-- Main Container -->
  

</div>

@endsection