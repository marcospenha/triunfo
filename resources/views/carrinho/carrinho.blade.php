@extends('layouts.app')

@section('content')

  <!-- Main Layout -->
  <main>

    <!-- Main Container -->
    <div class="container" ng-controller="carrinhoController">

      <!-- Section cart -->
      <section class="section my-5 pb-5">

        <div class="card card-ecommerce">

          <div class="card-body">

            <!-- Shopping Cart table -->
            <div class="table-responsive">

              <table class="table product-table table-cart-v-2">

                <!-- Table head -->
                <thead class="mdb-color lighten-5">

                  <tr>

                    <th></th>

                    <th class="font-weight-bold">

                      <strong>Produto</strong>

                    </th>

                    <th class="font-weight-bold">

                      <strong>Quantidade</strong>

                    </th>

                    <th></th>

                  </tr>

                </thead>
                <!-- Table head -->

                <!-- Table body -->
                <tbody>

                  <!-- First row -->
                  <tr ng-repeat="produto in produtosNoCarrinho track by $index">

                    <th scope="row">

                    <img src="/@{{produto.produto.foto_1}}" alt=""
                        class="img-fluid z-depth-0">

                    </th>

                    <td>

                      <h5 class="mt-3">

                        <strong>@{{produto.produto.nome}}</strong>

                      </h5>

                      <p class="text-muted">@{{produto.produto.marca}}</p>

                    </td>

                    <td class="text-center text-md-left">

                      <span class="qty">@{{produto.quantidade}}</span>

                      <div class="btn-group radio-group ml-2" data-toggle="buttons">

                        <label class="btn btn-sm btn-primary btn-rounded" ng-click="reduzirQuantidade($index)">

                          <input type="radio" name="option-@{{produto.produto.id}}" id="option-@{{produto.produto.id}}">&mdash;

                        </label>

                        <label class="btn btn-sm btn-primary btn-rounded" ng-click="aumentarQuantidade($index)" >

                          <input type="radio" name="option-@{{produto.produto.id}}" id="option-@{{produto.produto.id}}">+

                        </label>

                      </div>

                    </td>

                    <td>

                      <button type="button" class="btn btn-sm btn-primary" ng-click="removerItemCarrinho($index)" data-toggle="tooltip" data-placement="top"
                        title="Remove item">X

                      </button>

                    </td>

                  </tr>
                  <!-- First row -->

                </tbody>
                <!-- Table body -->

              </table>

            </div>
            <!-- Shopping Cart table -->

          </div>

        </div>

        <!-- Add to Cart -->
        <section class="color">

            <div class="mt-5">

                <div class="row mt-3 mb-4">

                  <div class="col-md-6 text-center text-md-left">

                      <a class="btn btn-primary btn-rounded" href="/">

                      Selecionar Mais Produtos</a>

                  </div>

                  <div class="col-md-6 text-center text-md-left text-md-right">

                      <button class="btn btn-primary btn-rounded" ng-click="concluirPedido({{ Auth::user()->id }})">

                      Solicitar Cotação</button>

                  </div>

                </div>

            </div>

            </section>
            <!-- Add to Cart -->

      </section>
      <!-- Section cart -->

    </div>
    <!-- Main Container -->

  </main>
  <!-- Main Layout -->

@endsection