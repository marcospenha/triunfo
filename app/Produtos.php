<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    protected $table = 'produtos';

    public function categorias()
    {
        return $this->hasOne('App\Categorias');
    }

    public function subcategorias()
    {
        return $this->hasOne('App\Subcategorias');
    }

}
