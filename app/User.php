<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Notifications\ResetPassword;

class User extends Authenticatable implements AuthenticatableContract,
CanResetPasswordContract
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cnpj',
        'email',
        'razao_social',
        'inscricao_estadual',
        'ramo_atividade',
        'cep',
        'estado',
        'cidade',
        'bairro',
        'endereco',
        'numero',
        'complemento',
        'ponto_referencia',
        'quantos_funcionario',
        'nome_completo',
        'data_nascimento',
        'sexo',
        'profissao',
        'telefone_fixo',
        'telefone_celular',
        'cep_entrega',
        'estado_entrega',
        'cidade_entrega',
        'bairro_entrega',
        'endereco_entrega',
        'numero_entrega',
        'complemento_entrega',
        'ponto_referencia_entrega',
        'password',
        'nome_fantasia'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
