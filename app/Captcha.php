<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Client\Response;

class Captcha extends Model
{
    
    public function verificarCaptcha($captcha){
        $secretKey = "6LeHj-AZAAAAAP8CBWElpk0usEu3I4-ooVLK_VM_";
      

        $client = new \GuzzleHttp\Client();
        $url = "https://www.google.com/recaptcha/api/siteverify";
       
    
      
        $response = $client->request('POST', $url, [
            'form_params' => [
                'secret' => $secretKey,
                'response' => $captcha
            ]
        ]);
  
      
        return $response->getStatusCode();;
    }

 
}
