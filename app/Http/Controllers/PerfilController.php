<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use App\User;
use DateTime;

class PerfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('perfil.perfil');
    }

    public function getUserInfo(){

        $userData =  Auth::user();

        $pedidos = DB::table('pedidos')->where('users_id', $userData->id)->get();

        $produtosCollection = collect([]);

        $pedidosCollection = collect(['pedido', 'produtos', 'valor_total']);

        foreach ($pedidos as &$item) {
           
            $produto = DB::table('pedidos_has_produtos')
                ->join('produtos', 'pedidos_has_produtos.produtos_id', '=', 'produtos.id')
                ->select('produtos.nome', 'produtos.id as produtos_id', 'produtos.marca', 'produtos.codigo_produto', 
                'pedidos_has_produtos.quantidade','pedidos_has_produtos.preco','pedidos_has_produtos.subtotal','pedidos_has_produtos.id as pedidos_has_produtos_id',
                'pedidos_has_produtos.aceito_pelo_cliente')
                ->where('pedidos_has_produtos.pedidos_id', $item->id)
                ->get();
            
            if ($item->status == 'Respondido') {
                $total_pedido = DB::table('pedidos_has_produtos')
                    ->join('produtos', 'pedidos_has_produtos.produtos_id', '=', 'produtos.id')
                    ->where('pedidos_has_produtos.pedidos_id', $item->id)
                    ->where('pedidos_has_produtos.preco', '>', 0)
                    ->sum('pedidos_has_produtos.subtotal');
            } else {
                $total_pedido = DB::table('pedidos_has_produtos')
                    ->join('produtos', 'pedidos_has_produtos.produtos_id', '=', 'produtos.id')
                    ->where('pedidos_has_produtos.pedidos_id', $item->id)
                    ->where('pedidos_has_produtos.aceito_pelo_cliente', 'S')
                    ->where('pedidos_has_produtos.preco', '>', 0)
                    ->sum('pedidos_has_produtos.subtotal');
            }
                
                
            $produtosCollection->push($pedidosCollection->combine([$item, $produto, number_format($total_pedido, 2, ',', '.')]));
            
        }

        $reponse = collect(['user', 'pedidos']);

        return $reponse->combine([$userData, $produtosCollection]);

    }

    public function aceitarProdutoCotacao($id){

        $pedidos_has_produtos = DB::table('pedidos_has_produtos')->where('id', $id)->get();

        if($pedidos_has_produtos[0]->aceito_pelo_cliente == "S"){
            $pedidos_has_produtos = DB::table('pedidos_has_produtos')->where('id', $id)->update(['aceito_pelo_cliente' => 'N']);
            return $pedidos_has_produtos;
        }
        if($pedidos_has_produtos[0]->aceito_pelo_cliente == "N"){
            $pedidos_has_produtos = DB::table('pedidos_has_produtos')->where('id', $id)->update(['aceito_pelo_cliente' => 'S']);
            return $pedidos_has_produtos;
        }

    }

    public function concluirCotacao($acao, $id){

        if ($acao == 'aceitar'){
            $pedido = DB::table('pedidos')->where('id', $id)->update(['status' => 'Pedido aceito pelo cliente']);
        } else if ($acao == 'reabrir'){
            $pedido =  DB::table('pedidos')->where('id', $id)->update(['status' => 'Pedido reaberto pelo cliente']);
            DB::table('pedidos_has_produtos')->where('pedidos_id', $id)->update(['preco' => null, 'subtotal' => null]);       
        } else if ($acao == 'repetir'){
            $pedido = DB::table('pedidos')->where('id', $id)->get();

            $date = \DateTime::createFromFormat('Y/m/d', $data->data_pedido);

            $pedido_id = DB::table('pedidos')->insertGetId(
                [
                    'users_id'   => $pedido[0]->users_id, 
                    'created_at' => new \DateTime($date),
                    'status'     => 'Solicitado'
                ]
            );
    
            $produtos = DB::table('pedidos_has_produtos')->where('pedidos_id', $id)->get();
            //echo $produtos
            // $result = json_decode($produtos, true);
            for($i=0; $i < count($produtos); $i++){
                if ($produtos[$i]->aceito_pelo_cliente == 'S') {
                    DB::table('pedidos_has_produtos')->insert([
                        [
                            'pedidos_id'  => $pedido_id, 
                            'produtos_id' => $produtos[$i]->produtos_id,
                            'quantidade'  => $produtos[$i]->quantidade,
                        ],
                        
                    ]);
                }                
            }
            //return $pedido[0]->users_id;
        }

        return $pedido;
    }

    public function concluirCotacaoRecusar(Request $request, $id){
        $pedido =  DB::table('pedidos')->where('id', $id)->update(['status' => 'Pedido recusado pelo cliente', 'motivo_recusao_cotacao' => $request->motivo]);
        return $pedido;
    }

    public function editar(Request $data){

        $user = User::find($data['id']);

        $user->cnpj = $data['cnpj'];
        $user->email = $data['email'];
        $user->razao_social = $data['razao_social'];
        $user->inscricao_estadual = $data['inscricao_estadual'];
        $user->ramo_atividade = $data['ramo_atividade'];
        $user->cep = $data['cep'];
        $user->estado = $data['estado'];
        $user->cidade = $data['cidade'];
        $user->bairro = $data['bairro'];
        $user->endereco = $data['endereco'];
        $user->numero = $data['numero'];
        $user->complemento = $data['complemento'];
        $user->ponto_referencia = $data['ponto_referencia'];
        $user->quantos_funcionario = $data['quantos_funcionario'];
        $user->nome_completo = $data['nome_completo'];
        // $user->data_nascimento = $data['data_nascimento'];
        $user->sexo = $data['sexo'];
        $user->profissao = $data['profissao'];
        $user->telefone_fixo = $data['telefone_fixo'];
        $user->telefone_celular = $data['telefone_celular'];
        $user->cep_entrega = $data['cep_entrega'];
        $user->estado_entrega = $data['estado_entrega'];
        $user->cidade_entrega = $data['cidade_entrega'];
        $user->bairro_entrega = $data['bairro_entrega'];
        $user->endereco_entrega = $data['endereco_entrega'];
        $user->numero_entrega = $data['numero_entrega'];
        $user->complemento_entrega = $data['complemento_entrega'];
        $user->ponto_referencia_entrega = $data['ponto_referencia_entrega'];
        // $user->password = Hash::make($data['password']);
        $user->nome_fantasia = $data['nome_fantasia']; 

        $user->save();

        return redirect('perfil');

    }
}
