<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\EmailService;
use Auth;

class PedidoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function carrinho(){
        return view('carrinho.carrinho');
    }

    public function concluirPedido(Request $data){

        $date = \DateTime::createFromFormat('Y/m/d', $data->data_pedido);

        $pedido_id = DB::table('pedidos')->insertGetId(
            [
                'users_id'   => $data->cliente, 
                'created_at' => new \DateTime($date),
                'status'     => 'Solicitado'
            ]
        );

        foreach ($data->produtos as &$item) {
           
            DB::table('pedidos_has_produtos')->insert([
                [
                    'pedidos_id'  => $pedido_id, 
                    'produtos_id' => $item['produto_id'],
                    'quantidade'  => $item['quantidade'],
                    'preco' => 0,
                    'subtotal' => 0
                ],
                
            ]);

        }
        $emailService = new EmailService();
        $emailService->emailSolicitacaoDeCotacao(Auth::user()->email, []);

        return response('Pedido nviado com sucesso', 200)
               ->header('Content-Type', 'text/plain');

    }

}
