<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailService;
use App\Captcha;
use Illuminate\Http\Client\Response;

class ContatoController extends Controller
{
    public function index(){
        return view('contato.contato');
    }

    public function enviarEmail(Request $data) {
        $captcha = $data['g-recaptcha-response'];
        
        $verificarCaptcha = new Captcha();
        $captchaCode = $verificarCaptcha->verificarCaptcha($captcha);
        
        if(empty($captcha) || $captchaCode != 200){
          return redirect()->back()->with('captcha_erro', trans('Selecione a caixa de captcha!'));
        }
        
        $emailService = new EmailService();
        $data = array(
            'nome'=>$data['nome'],
            'email'=>$data['email'],
            'telefone'=>$data['telefone'],
            'assunto'=>$data['assunto'],
            'mensagem'=>$data['mensagem']
        );
   
        $emailService->emailContato($data);

        return redirect()->back()->with('status', trans('Sua Mensagem foi enviada com sucesso, entraremos em contato o mais rápido possível.'));
    }
}
