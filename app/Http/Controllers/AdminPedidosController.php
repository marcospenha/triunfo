<?php namespace App\Http\Controllers;

	use Session;
	use DB;
	use CRUDBooster;
	use NumberFormatter;
	use Mail;
	use App\EmailService;
	use Illuminate\Http\Request;

	class AdminPedidosController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "pedidos";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Cliente","name"=>"users_id","join"=>"users,razao_social"];
			$this->col[] = ["label"=>"Data do Pedido","name"=>"created_at",'callback_php'=>'date("d/m/Y",strtotime($row->created_at))'];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"Número do Pedido","name"=>"codigo_pedido"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=> 'Status','name'=>'status','type'=>'select', 'dataenum'=>'Respondido;Pedido aceito pelo fornecedor;Pronto para entrega;Concluído;Pedido recusado pelo cliente;Pedido aceito pelo cliente;Solicitado'];
			$this->form[] = ['label'=>'Prazo de Entrega','name'=>'prazo_entrega','type'=>'date'];
			$this->form[] = ['label'=>'Condição de Pagamento','name'=>'condicao_pagamaento','type'=>'text'];
			$this->form[] = ['label'=>'Número do Pedido','name'=>'codigo_pedido','type'=>'text'];
			$this->form[] = ['label'=>'Número da Nota Fiscal','name'=>'numero_nota_fiscal','type'=>'text'];
			
			$columns[] = ['label'=>'Produto','name'=>'produtos_id','type'=>'datamodal','datamodal_table'=>'produtos','datamodal_columns'=>'nome,codigo_produto','datamodal_select_to'=>'codigo_produto:produtos_codigo_produto','datamodal_where'=>'','datamodal_size'=>'large'];		
			// $columns[] = ['label'=>'Código do Produto','name'=>'codigo_produto','type'=>'codigo_produto','datamodal_table'=>'produtos','readonly'=>true];   'formula'=>'([field1])*1+([field2]*1)
			$columns[] = ['label'=>'Preço','name'=>'preco','type'=>'number','required'=>true];
			$columns[] = ['label'=>'Quantidade','name'=>'quantidade','type'=>'number','required'=>true];
			$columns[] = ['label'=>'Aceito Pelo Cliente','name'=>'aceito_pelo_cliente','type'=>'text',"readonly"=>false, 'required'=>true];
			$columns[] = ['label'=>'Sub Total','name'=>'subtotal','type'=>'number','formula'=>"  (([quantidade])*1)*(([preco]*1))","readonly"=>true,'required'=>true, 'callback_php'=>'round($row->subtotal, 2)'];
			$this->form[] = ['label'=>'Detalhes de pedidos','name'=>'pedidos_has_produtos','type'=>'child','columns'=>$columns,'table'=>'pedidos_has_produtos','foreign_key'=>'pedidos_id'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Users Id","name"=>"users_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"users,id"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "
				$(function() {
					$(document).ready(function(){
						$('#detalhesdepedidosaceito_pelo_cliente').attr('value', 'N');
					});
					$(document).ready(function(){
						$('#detalhesdepedidospreco').attr('value', '0');
					});
					$(document).ready(function(){
						$('#detalhesdepedidosaceito_pelo_cliente').attr('onkeyup', 'this.value = this.value.toUpperCase()'); 
					});
					$(document).ready(function(){
						$('#detalhesdepedidosaceito_pelo_cliente').attr('maxlength','1'); 
					});
							
				});
			";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }

		public function getDetail($id) {
			//Create an Auth
			if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {    
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			$data = [];
			$data['page_title'] = 'Detalhes de Pedidos';
			$data['pedidos'] = DB::table('pedidos')->where('id', $id)->get();
			$data['cliente'] = DB::table('users')->where('id',$data['pedidos'][0]->users_id)->get();
			//return $data['cliente'];
			$data['produtos'] = DB::table('pedidos_has_produtos')
				->join('produtos', 'pedidos_has_produtos.produtos_id', '=', 'produtos.id')
				->select('produtos.nome', 'produtos.marca', 'produtos.codigo_produto', 'pedidos_has_produtos.quantidade', 
				'pedidos_has_produtos.aceito_pelo_cliente', 'pedidos_has_produtos.preco', 'pedidos_has_produtos.subtotal')
				->where('pedidos_has_produtos.pedidos_id', $data['pedidos'][0]->id)
					->where('pedidos_has_produtos.preco', '>', 0)
				->get();

			$total_pedido = DB::table('pedidos_has_produtos')
				->join('produtos', 'pedidos_has_produtos.produtos_id', '=', 'produtos.id')
				->where('pedidos_has_produtos.pedidos_id', $data['pedidos'][0]->id)
				->where('pedidos_has_produtos.preco', '>', 0)
				->sum('pedidos_has_produtos.subtotal');

			$data['valor_total'] = number_format($total_pedido, 2, ',', '.');
			//return $data;
			//Please use cbView method instead view method from laravel
			$this->cbView('custom_detail_view',$data);
		}

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
			//Your code here
		    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
			//Your code here 
			$emailService = new EmailService();
			$pedido = DB::table('pedidos')
					  ->join('users', 'pedidos.users_id', '=', 'users.id')
					  ->select('pedidos.id', 'pedidos.created_at', 'pedidos.status', 'users.email', 'users.razao_social', 'pedidos.status', 'pedidos.codigo_pedido', 'pedidos.numero_nota_fiscal')
					  ->where('pedidos.id', $id)
					  ->get();
			
			$data = array(
				'razao_social' => $pedido[0]->razao_social,
				'email' => $pedido[0]->email,
				'status' => $pedido[0]->status,
				'created_at' => $pedido[0]->created_at,
				'codigo_pedido' => $pedido[0]->codigo_pedido,
				'numero_nota_fiscal' => $pedido[0]->numero_nota_fiscal,
				'id' => $pedido[0]->id
			);

			//Respondido;Pedido aceito pelo fornecedor;Pronto para entrega;Concluído
			$email = $pedido[0]->email;
			
			if($pedido[0]->status == 'Respondido'){
				$assunto = 'Cotação – Loja Virtual Triunfo';
				$template = 'emails.envio_cotacao_email';
				$emailService->enviarEmail($assunto, $email, $data, $template);
			}
			if($pedido[0]->status == 'Pedido aceito pelo fornecedor'){
				$assunto = 'Pedido – Loja Virtual Triunfo';
				$template = 'emails.informacao_pedido';
				$emailService->enviarEmail($assunto, $email, $data, $template);
			}
			if($pedido[0]->status == 'Pronto para entrega'){
				$assunto = 'Pedido pronto para entrega – Loja Virtual Triunfo';
				$template = 'emails.pronto_entrega';
				$emailService->enviarEmail($assunto, $email, $data, $template);
			}
			
			
			
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }


	}