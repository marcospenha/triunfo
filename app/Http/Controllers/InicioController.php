<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InicioController extends Controller
{
    public function index(){
        return view('inicio.inicio');
    }

    public function itemDestaque(){
        $itemDestaque = DB::table('produto_destaque')
            ->where('ativo', 'Sim')
            ->get();

        return $itemDestaque;
    }

    public function buscar($query){
        $produtos = DB::table('produtos')
            ->where('nome', 'like', '%' . $query . '%')            
            ->orWhere('codigo_produto', 'like', '%' . $query . '%')
            ->orWhere('marca', 'like', '%' . $query . '%')
            ->get();

        return $produtos;
    }

    public function buscarView(){
        return view('buscar.buscar');
    }

    public function slides(){
        $slides =  DB::table('slides')->get();
        return response()->json($slides[0]);
    }

}
