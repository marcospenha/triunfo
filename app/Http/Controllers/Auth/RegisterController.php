<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\EmailService;
use App\Captcha;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $data)
    {
        
        $captcha = $data['g-recaptcha-response'];
        
        $verificarCaptcha = new Captcha();
        $captchaCode = $verificarCaptcha->verificarCaptcha($captcha);
        
        if(empty($captcha) || $captchaCode != 200){
          return redirect()->back()->with('captcha_erro', trans('Selecione a caixa de captcha!'));
        }
        
        $user = null;
        $emailService = new EmailService();

        $validatedData = $data->validate(
            [
                'email' => ['required', 'unique:users', 'max:255'],
            ],
            [
                'email.unique' => 'Este email já está cadastrado!',
            ]
        );

        $user = User::create([
            'cnpj' => $data['cnpj'],
            'email' => $data['email'],
            'razao_social' => $data['razao_social'],
            'inscricao_estadual' => $data['inscricao_estadual'],
            'ramo_atividade' => $data['ramo_atividade'],
            'cep' => $data['cep'],
            'estado' => $data['estado'],
            'cidade' => $data['cidade'],
            'bairro' => $data['bairro'],
            'endereco' => $data['endereco'],
            'numero' => $data['numero'],
            'complemento' => $data['complemento'],
            'ponto_referencia' => $data['ponto_referencia'],
            'quantos_funcionario' => $data['quantos_funcionario'],
            'nome_completo' => $data['nome_completo'],
            'data_nascimento' => $data['data_nascimento'],
            'sexo' => $data['sexo'],
            'profissao' => $data['profissao'],
            'telefone_fixo' => $data['telefone_fixo'],
            'telefone_celular' => $data['telefone_celular'],
            'cep_entrega' => $data['cep_entrega'],
            'estado_entrega' => $data['estado_entrega'],
            'cidade_entrega' => $data['cidade_entrega'],
            'bairro_entrega' => $data['bairro_entrega'],
            'endereco_entrega' => $data['endereco_entrega'],
            'numero_entrega' => $data['numero_entrega'],
            'complemento_entrega' => $data['complemento_entrega'],
            'ponto_referencia_entrega' => $data['ponto_referencia_entrega'],
            'password' => Hash::make($data['password']),
            'nome_fantasia' => $data['nome_fantasia'], 
        ]);

        $emailService->emailBoasVindas($user->email, array('nome' => $user->razao_social));
        Auth::login($user);
        return redirect('/');
    }


}
