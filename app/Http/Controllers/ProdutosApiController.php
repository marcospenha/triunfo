<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Produtos;
use App\Categorias;
use App\Subcategorias;

class ProdutosApiController extends Controller
{
  public function listarProdutosRandomico(){

    $produtos = DB::table('produtos')
                ->join('categorias', 'categorias.id', '=', 'produtos.categorias_id')
                ->join('subcategorias', 'subcategorias.id', '=', 'produtos.subcategorias_id')
                ->select('produtos.*', 'categorias.nome as categoria', 'subcategorias.nome as subcategoria')
                ->where('produtos.publicar', 'Sim')
                ->get();

    $produtosCollection = collect($produtos);
    $groupsProdutos = $produtosCollection->random(9);
    //
    return $groupsProdutos;

  }

  public function listarProdutosRecentes(){
    $produtos = DB::table('produtos')
                ->join('categorias', 'categorias.id', '=', 'produtos.categorias_id')
                ->join('subcategorias', 'subcategorias.id', '=', 'produtos.subcategorias_id')
                ->select('produtos.*', 'categorias.nome as categoria', 'subcategorias.nome as subcategoria')
                ->where('produtos.publicar', 'Sim')
                ->orderByRaw('produtos.updated_at - produtos.created_at ASC')
                ->take(4)
                ->get();

    return $produtos;
  }

  public function listarProduto($id){
    $produtos = Produtos::find($id);
    $categoria = Categorias::find($produtos->categorias_id);
    $subcategoria = Subcategorias::find($produtos->subcategorias_id);
    
    return view('produto.produto' ,['produtos' => $produtos, 'categoria' => $categoria, 'subcategoria' => $subcategoria]);
  }

  public function getProduto($id){
    return Produtos::find($id);
  }

  public function getCodigoProduto($id){
      $codigo_produto = DB::table('produtos')->select('codigo_produto')->where('id', $id)->get();
      return $codigo_produto[0]->codigo_produto;
  }

}
