<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriaApiController extends Controller
{
    public function listarCategorias(){

      $categorias = DB::table('categorias')->get();
      return $categorias;

    }

    public function listarCategoriaPorId($id){

      $categorias = DB::table('categorias')->where('id', $id)->get();
      return $categorias;

    }

    public function listarSubategorias($id){

      $subcategorias = DB::table('subcategorias')
                      ->join('categorias', 'categorias.id', '=', 'subcategorias.categorias_id')
                      ->select('subcategorias.*', 'categorias.nome as categoria')
                      ->where('categorias_id', $id)->get();
      return $subcategorias;

    }

    public function listaProdutosPorCategoria($id){

      $produtos = DB::table('produtos')
                  ->join('categorias', 'categorias.id', '=', 'produtos.categorias_id')
                  ->join('subcategorias', 'subcategorias.id', '=', 'produtos.subcategorias_id')
                  ->select('produtos.*', 'categorias.nome as categoria', 'subcategorias.nome as subcategoria')
                  ->where('produtos.publicar', 'Sim')
                  ->where('produtos.categorias_id', $id)->get();

      return $produtos;

    }

    public function listarCategoriasGrupos(){
      $categorias = DB::table('categorias')->count();
      $categoriasCollection = collect(DB::table('categorias')->get());
      $groupsCategorias = $categoriasCollection->split(ceil($categorias/5));

      return $groupsCategorias->toArray();
    }

}
