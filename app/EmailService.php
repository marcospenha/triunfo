<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;

class EmailService extends Model
{
    
    public function enviarEmail($assunto, $email, $data, $template){
        Mail::send(['text' => $template], $data, function($message) use ($email,$assunto) {
            $message->to($email, 'Contato Triunfo')->subject($assunto);
            $message->from(env('EMAIL'),'Loja Triunfo');
        });
    }

    public function emailBoasVindas($email, $data){
        Mail::send(['text' => 'emails.boas_vindas_email'], $data, function($message) use ($email) {
            $message->to($email, 'Contato Triunfo')->subject('Confirmação de seu Cadastro – Loja Virtual Triunfo');
            $message->from(env('EMAIL'),'Loja Triunfo');
        });
    }

    public function emailContato($data){
        Mail::send(['text'=>'emails.contato_email'], $data, function($message) {
            $message->to('vendas@triunfopapeis.com.br', 'Contato Triunfo')->subject('Contato Triunfo');
            $message->from(env('EMAIL'),'Contato');
         });
    }

    public function emailSolicitacaoDeCotacao($email, $data){
        Mail::send(['text'=>'emails.recebimento_solicitacao_email'], $data, function($message) use ($email) {
            $message->to($email, 'Contato Triunfo')->subject('Solicitação de Cotação Online – Loja Virtual Triunfo');
            $message->from(env('EMAIL'),'Contato');
         });
    }
}
