<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->float('preco');
            $table->integer('categorias_id');
            $table->integer('subcategorias_id');	
            $table->text('descricao');	
            $table->text('disponibilidade');	
            $table->binary('foto_1');	
            $table->binary('foto_2')->nullable();	
            $table->binary('foto_3')->nullable();	
            $table->binary('foto_4')->nullable();	
            $table->binary('foto_5')->nullable();	
            $table->binary('foto_6')->nullable();	
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
