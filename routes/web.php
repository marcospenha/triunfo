<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@index')->name('inicio');
Route::get('/produtos/resultado/buscar', 'InicioController@buscarView');
Route::get('/produtos/resultado/buscar/{query}', 'InicioController@buscar');
Route::get('/contato', 'ContatoController@index')->name('contato');
Route::get('/categoria/{slug}', 'CategoriasController@index')->name('contato');

Route::post('cadastrar', 'Auth\RegisterController@create')->name('cadastrar');
Route::post('editar', 'PerfilController@editar')->name('editarCadastro');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/item/destaque', 'InicioController@itemDestaque');
Route::get('/item/slides', 'InicioController@slides');

Route::get('/listar/categorias', 'CategoriaApiController@listarCategorias');
Route::get('/listar/categorias/grupos', 'CategoriaApiController@listarCategoriasGrupos');
Route::get('/listar/categoria/{id}', 'CategoriaApiController@listarCategoriaPorId');
Route::get('/listar/categoria/subcategoria/{id}', 'CategoriaApiController@listarSubategorias');
Route::get('/listar/categoria/produtos/{id}', 'CategoriaApiController@listaProdutosPorCategoria');

Route::get('/listar/produtos/randomico', 'ProdutosApiController@listarProdutosRandomico');
Route::get('/listar/produtos/recentes', 'ProdutosApiController@listarProdutosRecentes');
Route::get('/produto/{id}/{slug}', 'ProdutosApiController@listarProduto');
Route::get('/listar/produto/{id}', 'ProdutosApiController@getProduto');
Route::get('/listar/produto/{id}/codigo', 'ProdutosApiController@getCodigoProduto');

Route::get('/perfil', 'PerfilController@index')->name('perfil'); 
Route::get('/perfil/data', 'PerfilController@getUserInfo')->name('getUserInfo');
Route::get('/perfil/aceitar/produto/{id}', 'PerfilController@aceitarProdutoCotacao');
Route::get('/perfil/concluir/cotacao/{acao}/{id}', 'PerfilController@concluirCotacao');
Route::post('/perfil/concluir/cotacao/recusar/{id}', 'PerfilController@concluirCotacaoRecusar');
Route::get('/cotacao', 'PedidoController@carrinho')->name('carrinho');

Route::post('/pedidos/concluir', 'PedidoController@concluirPedido')->name('pedidosConcluir');

Route::post('/email/enviar', 'ContatoController@enviarEmail')->name('enviarEmail');

Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@resetPassword')->name('password.editar');
// Route::post('reset_password_without_token', 'AccountsController@validatePasswordRequest');
// Route::post('reset_password_with_token', 'AccountsController@resetPassword');
