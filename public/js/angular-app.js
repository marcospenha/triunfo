var app = angular.module("triungoApp", ["slugifier","ui.utils.masks"]);

app.controller("inicioController", function($scope, $http, $rootScope,CategoriaService) {

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  $scope.submitBusca = function() {
    location.href = "/produtos/resultado/buscar/?q="+$scope.busca;
  };

  $rootScope.categoriaID = 0;
  $http.get('/listar/categorias').
    then(function(response) {
      $scope.status = response.status;
      $scope.categoriasLista = response.data;
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $http.get('/item/slides').
    then(function(response) {
      $scope.status = response.status;
      $scope.slides = response.data;

      
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $http.get('listar/produtos/recentes').
    then(function(response) {
      $scope.status = response.status;
      $scope.produtosRecentesLista = response.data;
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $http.get('listar/produtos/randomico').
  then(function(response) {
      $scope.status = response.status;
      $scope.produtosLista = response.data;
      max = $scope.produtosLista.length - 9;
      $scope.exibirProduto = getRandomIntInclusive(1, max);      
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $http.get('item/destaque').
    then(function(response) {
      $scope.status = response.status;
      $scope.itemDestaque = response.data[0];
      if(angular.isUndefined($scope.itemDestaque)){
        $scope.itemDestaque = false;
      }
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });
  

  $scope.getCategoriaID = function(id){    
    CategoriaService.removeCategoriaId();
    CategoriaService.setCategoriaId(id);
  };
  

});

app.controller("categoriasController", function($scope, $http, $rootScope, CategoriaService, CarrinhoService) {
  $scope.subcategoriaFiltro = '';

  $http.get('/listar/categorias/grupos').
    then(function(response) {
      $scope.status = response.status;
      $scope.categoriasListaTodas = response.data;
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $http.get('/listar/categoria/subcategoria/'+CategoriaService.getCategoriaId()).
    then(function(response) {
      $scope.status = response.status;
      $scope.subcategoriasLista = response.data;
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  $http.get('/listar/categoria/produtos/'+CategoriaService.getCategoriaId()).
  then(function(response) {
      $scope.status = response.status;
      $scope.produtosListas = response.data;      
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });
  
  $scope.setValue = function(valor) { $scope.subcategoriaFiltro = valor; }
  
  $scope.getCategoriaID = function(id){    
    CategoriaService.removeCategoriaId();
    CategoriaService.setCategoriaId(id);
  };

  $scope.inserirCarrinho = function(produto){
    
    CarrinhoService.inserirCarrinho(produto);
    window.location = "/cotacao";
  };

});

app.controller("buscaController", function($scope, $http, $location, CategoriaService, CarrinhoService) {
  $scope.subcategoriaFiltro = '';

  $scope.query = window.location.search.replace('?q=','');
   
  function buscarProdutoQuery (){
    $http.get('/produtos/resultado/buscar/'+$scope.query ).
      then(function(response) {
        $scope.query = $scope.query.replace("%20", " ");
        $scope.status = response.status;
        $scope.produtosListas = response.data;
        
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
    });

  }

  buscarProdutoQuery();

  $scope.realizarBusca = function(){
    buscarProdutoQuery();
  };

  $http.get('/listar/categorias/grupos').
    then(function(response) {
      $scope.status = response.status;
      $scope.categoriasListaTodas = response.data;
    }, function(response) {
      $scope.data = response.data || 'Request failed';
      $scope.status = response.status;
  });

  // $http.get('/listar/categoria/subcategoria/'+CategoriaService.getCategoriaId()).
  //   then(function(response) {
  //     $scope.status = response.status;
  //     $scope.subcategoriasLista = response.data;
  //   }, function(response) {
  //     $scope.data = response.data || 'Request failed';
  //     $scope.status = response.status;
  // });

  // $http.get('/listar/categoria/produtos/'+CategoriaService.getCategoriaId()).
  // then(function(response) {
  //     $scope.status = response.status;
  //     $scope.produtosListas = response.data;      
  //   }, function(response) {
  //     $scope.data = response.data || 'Request failed';
  //     $scope.status = response.status;
  // });
  
  // $scope.setValue = function(valor) { $scope.subcategoriaFiltro = valor; }
  
  // $scope.getCategoriaID = function(id){    
  //   CategoriaService.removeCategoriaId();
  //   CategoriaService.setCategoriaId(id);
  // };

  $scope.inserirCarrinho = function(produto){
    
    CarrinhoService.inserirCarrinho(produto);
    window.location = "/cotacao";
  };

});

app.controller("cadastroController", function($scope, $http, $rootScope, CategoriaService) {

  $scope.checkboxModel = false;
  $scope.subcategoriaFiltro = 0;
  $scope.copiarEnderecoModel = false;

  $scope.getEndereco = function(){
    $http.get('https://viacep.com.br/ws/'+$scope.form.cep+'/json/').
      then(function(response) {     
                  
        $scope.form.estado = response.data.uf;
        $scope.form.cidade = response.data.localidade;
        $scope.form.bairro = response.data.bairro; 
        $scope.form.endereco = response.data.logradouro;
        $scope.form.complemento = response.data.complemento;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
    });
  };

  $scope.getEnderecoEntrega = function(){
    $http.get('https://viacep.com.br/ws/'+$scope.form.cep_entrega+'/json/').
      then(function(response) {
        $scope.form.estado_entrega = response.data.uf;
        $scope.form.cidade_entrega = response.data.localidade;
        $scope.form.bairro_entrega = response.data.bairro; 
        $scope.form.endereco_entrega = response.data.logradouro;
        $scope.form.complemento_entrega = response.data.complemento;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
    });
  };

  $scope.copiarEndereco = function(){
    $scope.copiarEnderecoModel = !$scope.copiarEnderecoModel;
   
    if($scope.copiarEnderecoModel){
      $scope.form.cep_entrega = $scope.form.cep;
      $scope.form.numero_entrega = $scope.form.numero;
      $scope.form.estado_entrega = $scope.form.estado;
      $scope.form.cidade_entrega = $scope.form.cidade;
      $scope.form.bairro_entrega = $scope.form.bairro; 
      $scope.form.endereco_entrega = $scope.form.endereco;
      $scope.form.complemento_entrega =  $scope.form.complemento;
    }else{
      $scope.form.estado_entrega = "";
      $scope.form.cidade_entrega = "";
      $scope.form.bairro_entrega = ""; 
      $scope.form.endereco_entrega ="";
      $scope.form.complemento_entrega = "";
    }

  };

});

app.controller("produtoController", function($scope, $http, $rootScope, CarrinhoService) {

  $scope.inserirCarrinho = function(id){
    
    $http.get('/listar/produto/'+id).
      then(function(response) {
          
          CarrinhoService.inserirCarrinho(response.data);
          window.location = "/cotacao";
        }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
      });


  };


});

app.controller("carrinhoController", function($scope, $http, $rootScope, CarrinhoService) {

  $scope.produtosNoCarrinho = CarrinhoService.getCarrinho();
    
  $scope.reduzirQuantidade = function(index){   
    if($scope.produtosNoCarrinho[index].quantidade > 1){
      $scope.produtosNoCarrinho[index].quantidade--;
      CarrinhoService.atualizarCarrinho($scope.produtosNoCarrinho);
    }
  };
  
  $scope.aumentarQuantidade = function(index){
    $scope.produtosNoCarrinho[index].quantidade++;
    CarrinhoService.atualizarCarrinho($scope.produtosNoCarrinho);
  };

  $scope.removerItemCarrinho = function(index){
    $scope.produtosNoCarrinho.splice(index,1);
    CarrinhoService.atualizarCarrinho($scope.produtosNoCarrinho);
  };
  
  $scope.concluirPedido = function(cliente_id) {
    var isConfirmed = confirm("Você tem certeza de que quer concluir a  cotação ?");
    if(isConfirmed){
      var dNow = new Date();
      var localdate = dNow.getDate() + '/' + (dNow.getMonth()+1) + '/' + dNow.getFullYear();
  
      var pedido = {
        cliente: cliente_id,
        produtos: $scope.produtosNoCarrinho,
        data_pedido: localdate
      };
    
  
      $http.post('/pedidos/concluir', pedido).
        then(function(response) {
          if(response.status === 200){
            CarrinhoService.removeCarrinho();
            //redirecionar para pagina de perfil
            window.location = "/perfil";
          }
  
        }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
      });
  
    }else{
      return false;
    }


  };

});

app.controller("perfilController", function($scope, $http) {

  $http.get('/perfil/data').
    then(function(response) {
       
        $scope.userData = response.data.user;
        
        angular.forEach(response.data.pedidos, function(value) {
          
          if(value.pedido.status === 'Pedido aceito pelo cliente' || value.pedido.status === 'Pedido aceito pelo fornecedor' || value.pedido.status === 'Concluído'){
            
            angular.forEach(value.produtos, function(item, key) {
              if(item.aceito_pelo_cliente === "N")
              value.produtos.splice(key,1);
            });
          }
          
        });
        
        $scope.pedidos = response.data.pedidos;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
  });

  $scope.aceitarProduto = function(produto){
   //console.log(produto);
   
    $http.get('/perfil/aceitar/produto/'+produto.pedidos_has_produtos_id).
      then(function(response) {        
                  
        }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
    });
    
  }

  $scope.aceitarCotacao = function(pedido){
    
    $http.get('/perfil/concluir/cotacao/aceitar/'+pedido.id).
      then(function(response) {        
          pedido.status = 'Pedido aceito pelo cliente';
          location.reload();
      }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
    });
    
  }

  $scope.recusarCotacao = function(pedido){
    let motivo = prompt('Qual o motivo de recusar a cotação?');

    if (motivo === null) {
      return;
    } else {
      var data = {motivo: motivo};
    }
        
    $http.post('/perfil/concluir/cotacao/recusar/'+pedido.id, data).
      then(function(response) {                     
          pedido.status = 'Pedido recusado pelo cliente';     
          location.reload();   
        }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
    });
    
  }

  $scope.reabrirCotacao = function(pedido){
    
    $http.get('/perfil/concluir/cotacao/reabrir/'+pedido.id).
      then(function(response) {        
        pedido.status = 'Pedido reaberto pelo cliente';    
        location.reload();
        }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
    });
    
  }

  $scope.repetirCotacao = function(pedido){
    $http.get('/perfil/concluir/cotacao/repetir/'+pedido.id).
      then(function(response) {                          
          location.reload();   
        }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
    });
  }

  $scope.checkAll = function(pedido) {
    
    angular.forEach(pedido.produtos, function(produto) {    
      produto.aceito_pelo_cliente = 'S';
      $scope.aceitarProduto(produto);
    });

    $scope.aceitarCotacao(pedido.pedido);
  };

});

app.service('CategoriaService', function () {
  
  this.getCategoriaId = function () {
    var id = localStorage.getItem('categoriaId');
    return parseInt(id);
  };

  this.setCategoriaId = function(value) {
    localStorage.setItem('categoriaId', value);
    return;
  };

  this.removeCategoriaId = function() {
    localStorage.removeItem('categoriaId');
    return;
  };
 
});

app.service('CarrinhoService', function () {
  
  this.inserirCarrinho = function (produto) {

    var carrinho = JSON.parse(localStorage.getItem('carrinho'));

    if(carrinho == null){
      carrinho = [];
      var item = {produto: produto, quantidade: 1, produto_id: produto.id};
      carrinho.push(item);
      localStorage.setItem('carrinho', JSON.stringify(carrinho));
    }else{

      var index = carrinho.findIndex(val => val.produto_id == produto.id);
     
      if (index > -1) {
        var novaQuantidade = carrinho[index].quantidade + 1;
        carrinho.splice(index,1);        
        var item = {produto: produto, quantidade: novaQuantidade, produto_id: produto.id};
        carrinho.push(item);
        localStorage.removeItem('carrinho');  
        localStorage.setItem('carrinho', JSON.stringify(carrinho));
      }       
      else {
        var item = {produto: produto, quantidade: 1, produto_id: produto.id};
        carrinho.push(item);
        localStorage.removeItem('carrinho');  
        localStorage.setItem('carrinho', JSON.stringify(carrinho));
      }

    }

  };

  this.getCarrinho = function() {
    var carrinho = JSON.parse(localStorage.getItem('carrinho'));
    return carrinho;
  };

  this.atualizarCarrinho = function(carrinho) {
    localStorage.removeItem('carrinho'); 
    localStorage.setItem('carrinho', JSON.stringify(carrinho));
  };

  this.removeCarrinho = function() {
    localStorage.removeItem('carrinho');
    return;
  };
 
});

app.directive('inputChange', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      scope.pedido.pedido.created_at = new Date(scope.pedido.pedido.created_at);
    }
  };
});